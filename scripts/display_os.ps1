<#

	.SYNOPSIS
	Skeleton script for my tiny powershell framework

	.DESCRIPTION
	Tiny powershell framework.

	To ease programming here is debugging levels :
	    -v :     display VERBOSE level messages
		-d :	 display DEBUG level messages
		-dev :   display DEVEL level messages (including DEBUG ones)
		-trace : display the line of script currently executed as well as DEVEL and DEBUG level messages
		-ask :   ask user before each execution
		-q :	silence all displays

	.PARAMETER h
	display help screen. Use Get-Help instead.

	.PARAMETER v
	enable verbose mode

	.PARAMETER d
	enable debug mode

	.PARAMETER dev
	enable devel mode

	.PARAMETER trace
	.enable trace mode. With this mode on you can trace entering and leaving every single function that use the Write-EnterFunction and Write-LeaveFunction calls.
	Very useful while developing a new script.

	.PARAMETER ask
	ask for each action

	.PARAMETER quiet
	quiet output completely

	.PARAMETER log
	log calls to e*() functions into specified logfile.
	If used in conjunction with -trace, it will use PowerShell Start-Transcript to log everything, including output of commands.
	Useful if you can't see the output of script for whatever reason. In this case, Write-ToLog() is deactivated.

	.NOTES
	Author: Charles-Antoine Degennes <cadegenn@gmail.com>

	.LINK
		https://github.com/cadegenn/pwsh_fw
#>

[CmdletBinding()]Param(
	[switch]$h,
	[switch]$v,
	[switch]$d,
	[switch]$dev,
	[switch]$trace,
	[switch]$ask,
	[switch]$quiet,
	[ValidateScript({
		Test-Path -Path $_ -PathType container
	})][string]$api,
	[string]$log,
	[ValidateScript({
		Test-Path -Path $_ -PathType leaf
	})][string]$configFile,
	[switch]$Force
)

$Global:BASENAME = Split-Path -Leaf $MyInvocation.MyCommand.Definition
$Global:VERBOSE = $v
$Global:DEBUG = $d
$Global:DEVEL = $dev
$Global:TRACE = $trace
$Global:ASK = $ask
$Global:QUIET = $quiet
$Global:LOG = $log
$rc = Get-Module -Name PwSh.Fw.Core
if (!$rc) {
	Install-Module -Name PwSh.Fw.Core -Confirm:$false
	$rc = Import-Module PwSh.Fw.Core -ErrorAction Stop -DisableNameChecking
}
$modules = @()

if ($h) {
	Get-Help $MyInvocation.MyCommand.Definition
	Exit
}

# keep the order as-is please !
$oldDebugPreference = $DebugPreference
$oldVerbosePreference = $VerbosePreference
$oldInformationPreference = $InformationPreference
if ($ASK)   { Set-PSDebug -Step }
if ($TRACE) {
	# Set-PSDebug -Trace 1
	$Global:DEVEL = $true
}
if ($DEVEL) {
	$Global:DEBUG = $true;
}
if ($DEBUG) {
	$DebugPreference="Continue"
	$Global:VERBOSE = $true
}
if ($VERBOSE) {
	$VerbosePreference="Continue"
}
if ($QUIET) {
	$Global:DEVEL = $false
	$Global:DEBUG = $false;
	$Global:VERBOSE = $false
}

if ($log) {
	if ($TRACE) {
		Start-Transcript -Path $log
	# } else {
	# 	# add -Append:$false to overwrite logfile
	# 	# Write-ToLogFile -Message "Initialize log" -Append:$false
	# 	Write-ToLogFile -Message "Initialize log"
	}
	$modules += "PwSh.Fw.Log"
}

# write-output "Language mode :"
# $ExecutionContext.SessionState.LanguageMode

#
# Load Everything
#
everbose("Loading modules")
# $modules += "PsIni"
# $modules += "PwSh.ConfigFile"
# $modules += "Microsoft.PowerShell.Archive"
# USER MODULES HERE
$ERRORFOUND = $false
ForEach ($m in $modules) {
	$rc = Load-Module -Name $m -Force:$Force
	if ($rc -eq $false) { $ERRORFOUND = $true }
}
if ($ERRORFOUND) { efatal("At least one module could not be loaded.") }

#############################
## YOUR SCRIPT BEGINS HERE ##
#############################

<#

  ######  ########    ###    ########  ########
 ##    ##    ##      ## ##   ##     ##    ##
 ##          ##     ##   ##  ##     ##    ##
  ######     ##    ##     ## ########     ##
       ##    ##    ######### ##   ##      ##
 ##    ##    ##    ##     ## ##    ##     ##
  ######     ##    ##     ## ##     ##    ##

#>

Install-Module -Name PwSh.Fw.Core -Confirm:$false -Force -AllowPrerelease
# Install-Module -Name PwSh.Fw.OS -Confirm:$false -Force -AllowPrerelease
# use development module
Import-Module -FullyQualifiedName $PSScriptRoot/../PwSh.Fw.OS/PwSh.Fw.OS.psd1 -Force -DisableNameChecking
$os = Get-OperatingSystem -Online
$os

<#

 ######## ##    ## ########
 ##       ###   ## ##     ##
 ##       ####  ## ##     ##
 ######   ## ## ## ##     ##
 ##       ##  #### ##     ##
 ##       ##   ### ##     ##
 ######## ##    ## ########

#>

#############################
## YOUR SCRIPT ENDS   HERE ##
#############################

if ($log) {
	if ($TRACE) {
		Stop-Transcript
	} else {
		Write-ToLogFile -Message "------------------------------------------"
	}
}

# reinit values
$Global:DebugPreference = $oldDebugPreference
$Global:VerbosePreference = $oldVerbosePreference
$Global:InformationPreference = $oldInformationPreference
Set-PSDebug -Off
