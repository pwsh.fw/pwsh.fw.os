[CmdletBinding()]Param(
	[switch]$v,
	[switch]$d,
	[switch]$dev,
	[switch]$trace,
	[switch]$SkipNuGet,
	[switch]$Force
)

$ErrorActionPreference = "Stop"

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
Write-Host -ForegroundColor Blue ">> header"

if (-not ($SkipNuGet)) {
	if (!(Get-PackageProvider -Name NuGet)) { Install-PackageProvider -Name NuGet -Force:$Force -Confirm:$false }
	Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	Install-Module PwSh.Fw.Core -ErrorAction stop -Force:$Force -AllowClobber -AllowPrerelease -Scope CurrentUser
	Install-Module PwSh.Fw.BuildHelpers -ErrorAction stop -Force:$Force -AllowClobber -AllowPrerelease -Scope CurrentUser
}
Import-Module PwSh.Fw.Core -Force:$Force -ErrorAction stop -DisableNameChecking -Verbose:$false
Import-Module PwSh.Fw.BuildHelpers -ErrorAction stop
Set-PwShFwConfiguration -i -v:$v -d:$d -dev:$dev -trace:$trace -OverridePSPreferences
Set-PwShFwDisplayConfiguration -MessageDisplayFormat " * {1,1}{2}" -RCPosition FLOW

$PSVersionTable
Get-Module PwSh.Fw.* | Format-Table
$project = Get-Project -Path $ROOTDIR
$project | Format-Table Name, Value
# $project

Write-Host -ForegroundColor Blue "<< header"
