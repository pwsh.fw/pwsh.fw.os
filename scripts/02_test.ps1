[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force,
	[ValidateSet('All', 'ScriptAnalyzer', 'Pester')]$Passes = 'All'
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

# $ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

if (($Passes -eq 'All') -or ($Passes -eq 'ScriptAnalyzer')) {
	Install-Module PsScriptAnalyzer
	Import-Module PSScriptAnalyzer
	Invoke-ScriptAnalyzer $ROOTDIR -Recurse
}

if (($Passes -eq 'All') -or ($Passes -eq 'Pester')) {
	Install-Module Pester -SkipPublisherCheck -RequiredVersion 4.10.0
	Install-Module PesterMatchHashtable
	Import-Module Pester -MaximumVersion 4.10.0
	Get-Module Pester | Format-Table Name, Version, ExportedFunctions
	# Invoke-Pester $ROOTDIR
	$files = Get-ChildItem $ROOTDIR -File -Recurse -Include *.psm1
	Invoke-Pester $ROOTDIR -Codecoverage $files
}

Write-Host -ForegroundColor Blue "<< $BASENAME"
