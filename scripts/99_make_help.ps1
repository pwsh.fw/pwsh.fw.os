[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

$PSVersionTable
Get-Module PwSh.Fw.* | Format-Table
$project = Get-Project -Path $ROOTDIR
$project | Format-Table Name, Value
# $project

Write-Host -ForegroundColor Blue "<< header"
Write-Verbose "Load PlatyPS"
Install-Module -Name platyPS
try {
	Import-Module platyPS -ErrorAction SilentlyContinue
} catch {
	Write-Error $_
}

Update-ModuleManifestRecurse -FullyQualifiedName $ROOTDIR/$($project.Name)/$($project.Name).psm1 -Metadata $project -Recurse -Confirm:$false
Update-ModuleManifest -Path $ROOTDIR/$($project.Name)/$($project.Name).psd1 -ModuleVersion $($project.Version) -PreRelease $($project.PreRelease)

Write-Verbose "Build help"
# git clone https://gitlab-ci-token:${CI_BUILD_TOKEN}@gitlab.com/PwSh.Fw/$($project.Name).wiki.git $ROOTDIR/../$($project.Name).wiki
if (-not (Test-Path $ROOTDIR/../$($project.Name).wiki)) {
	git clone git@gitlab.com:pwsh.fw/$($project.Name.ToLower()).wiki.git $ROOTDIR/../$($project.Name).wiki
}
if (Test-Path $ROOTDIR/../$($project.Name).wiki) {
	Push-Location $ROOTDIR/../$($project.Name).wiki
	Write-Verbose "PWD = $(Get-Location)"
	git pull
	Pop-Location
	Get-ChildItem -Path $ROOTDIR -Recurse -Name "*.psm1" | ForEach-Object {
		Write-Debug "Processing $_"
		$file = Get-Item $ROOTDIR/$_
		Import-Module -FullyQualifiedName $file.FullName
		New-MarkdownHelp -Module $($file.BaseName) -OutputFolder "$ROOTDIR/../$($project.Name).wiki/References/$($file.BaseName)" -Force
	}
	Push-Location $ROOTDIR/../$($project.Name).wiki
	Update-MarkdownHelp "./References"
	# git push https://gitlab-ci-token:${CI_BUILD_TOKEN}@gitlab.com/PwSh.Fw/$($project.Name).wiki.git
	git add References/*
	# git commit -am "wiki: update auto-generated documentation"
	# git push
	Pop-Location
	Write-Verbose "PWD = $(Get-Location)"
} else {
	Write-Error "Path '$ROOTDIR/../$($project.Name).wiki' not found. An error occured."
}

Write-Verbose "Clean"
# remove all manifest
Get-ChildItem -Recurse -Filter "*.psd1" | Remove-Item
