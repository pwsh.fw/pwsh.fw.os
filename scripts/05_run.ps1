[CmdletBinding()]Param(
)

. $PSScriptRoot/00_header.ps1

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

$RETRIES = 10
for ($i=1; $i -le $RETRIES; $i++) {
	if ($env:CI_COMMIT_BRANCH -eq "master") {
		$Version = "$($project.Version)"
	} else {
		$Version = "$($project.Version)$($project.preRelease)"
	}
	Install-Module PwSh.Fw.OS -AllowPrerelease -MinimumVersion $Version -Force
	$rc = $?
	if ($rc -eq $true) { break }
}

if ($rc -ne $true) { Write-Fatal "Latest pre-release version '$Version' not found on PSGallery." }

# get trace to eventually debug things
$Global:QUIET = $false
$Global:VERBOSE = $true
$Global:DEBUG = $true
$Global:DEVEL = $true
$Global:TRACE = $true
Import-Module PwSh.Fw.OS
$os = Get-OperatingSystem -Online
$os | Sort-ByProperties

Write-Host -ForegroundColor Blue "<< $BASENAME"
