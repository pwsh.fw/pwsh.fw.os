# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-09-04

### Added

#### All

-	`Set-ComputerNextBoot`

#### Linux

-	`Set-ComputerNextBootUEFI`
-	`Get-DiskPartDeviceName`
-	`Get-UEFIBootEntry`

#### Windows

-	`Get-ComputerBootMode`
-	`Set-ComputerNextBootBIOS`
-	`Set-ComputerNextBootUEFI`
-	`Get-UEFIBootEntry`

### Changed

-	`Load-Dictionaries`: add Prefix parameter to permit loading custom dictionnaries without overlapping modules names

## [1.1.1] - 2021-11-26

### Fixed

-	`OS.WinNT`: fix `Get-OSCurrentMajorVersionNumber`

### Security

## [1.1.0] - 2021-10-13

### Added

-	Functions to detect BIOS/UEFI boot mode
-	`Set-ComputerNextBootBIOS` function for Windows and Linux

### Changed

-	`Dict.Windows`: Improved `Mount-OfflineWindowsRegistry` and `Dismount-OfflineWindowsRegistry`
-	`Dict.Windows`: fix `Get-OSDistrib`

## [1.0.0] - 2020-10-02

-	detect running Windows, Linux, macOS operating system
-	can load dictionaries from custom location
-	Publish to [PowerShell Gallery](https://www.powershellgallery.com/packages/PwSh.Fw.OS)
