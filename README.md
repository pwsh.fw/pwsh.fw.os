[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b95ad686a8d04676b39f7fd036d36083)](https://www.codacy.com/gl/pwsh.fw/pwsh.fw.os?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=pwsh.fw/pwsh.fw.os&amp;utm_campaign=Badge_Grade)


[![Pipeline status - master](https://img.shields.io/gitlab/pipeline/pwsh.fw/pwsh.fw.os/master?label=build%20master&logo=gitlab)](https://gitlab.com/pwsh.fw/pwsh.fw.os/pipelines)
[![Pipeline status - develop](https://img.shields.io/gitlab/pipeline/pwsh.fw/pwsh.fw.os/develop?label=build%20develop&logo=gitlab)](https://gitlab.com/pwsh.fw/pwsh.fw.os/pipelines)

[![PowerShell Gallery - Version](https://img.shields.io/powershellgallery/v/PwSh.Fw.OS)](https://www.powershellgallery.com/packages/PwSh.Fw.OS)
[![PowerShell Gallery](https://img.shields.io/powershellgallery/dt/PwSh.Fw.OS)](https://www.powershellgallery.com/packages/PwSh.Fw.OS)
[![Powershell Platform](https://img.shields.io/powershellgallery/p/PwSh.Fw.OS)](https://www.powershellgallery.com/packages/PwSh.Fw.OS)

<img align="left" width="48" height="48" src="images/favicon.png">

# PwSh.Fw.OS

PwSh Framework OS module.

:warning: it is not available right now. It is in early stage of coding.

`PwSh.Fw.OS` is able to detect any OS on any target partition or drive. It can then provide running script/shell with dictionaries according to detected OS.

## Start using it

Just load the module, and ask your operating system

```powershell
Install-Module PwSh.Fw.OS -Scope AllUsers
Import-Module PwSh.Fw.OS
$os = Get-OperatingSystem -Online
```