# OS

## How to use it

Open a `Tiny {PowerShell} Framework` console

To get informations about running operating system, type the following

```powershell
Import-Module PwSh.OS
$os = Get-OperatingSystem -Online
```

> Note that if your running OS is Windows and the system drive is `C:`, these two commands are equivalent :
>
> ```powershell
> $os = Get-OperatingSystem -Online
> $os = Get-OperatingSystem -Root 'C:'
> ```
>
> The same apply for a Unix / Linux OS, these two commands are equivalent as well :
>
> ```powershell
> $os = Get-OperatingSystem -Online
> $os = Get-OperatingSystem -Root '/'
> ```

It is also possible to get informations about an offline operating system
located on another disk. To achieve this, your currently running OS may need
some dictionaries to be able to fully identify and use offline OS. So it is
advised to first declare your online OS, load dictionaries, and then get an
offline operating system. Example as follow :

```powershell
Import-Module PwSh.OS
$os = Get-OperatingSystem -Online
$os | Load-Dictionaries
$offline = Get-OperatingSystem -Root 'D:\'
```

It also works in Linux :

```powershell
Import-Module PwSh.OS
$os = Get-OperatingSystem -Online
$os.loadDictionaries()
$offline = Get-OperatingSystem -Root '/mnt/another-root'
```

## How it works

### Hierarchy of Operating System

To make the code versatile, we use dictionaries. To be able to use correct
dictionary in all case, we have to be aware of the Operating System hierarchy.
Dictionaries are loaded in a specific order, like a breadcrumb. This order is,
from the more generic to the more specific :

```code
$os.platform > $os.kernel > $os.family > $os.distrib > $os.ReleaseId
```

As a reminder for myself, here is a table showing what `$os` attributes or methods _should_ return (this table may change over time) :

| Platform | Kernel | Family     | Distrib        | ReleaseId | Version | Name                  | ProductName                                 |
| -------- | ------ | ---------  | -------------- | --------- | ------- | --------------------- | ------------------------------------------- |
| Unix     | Linux  | Debian     | Ubuntu         | 16.04     | 16.04.5 | Ubuntu 16.04          | Ubuntu 16.04.5 LTS                          |
| Unix     | Linux  | Debian     | Ubuntu         | 18.04     | 18.04.1 | Ubuntu 18.04          | Ubuntu 18.04.1 LTS                          |
| Unix     | Linux  | Debian     | LinuxMint      | 18.03     | 18.03.1 | LinuxMint 18.03       | LinuxMint 18.03 LTS                         |
| Unix     | Linux  | Debian     | Raspbian       | 9         | 9.4     | Raspbian 9.4          | Raspbian GNU/Linux (debian/jessie)          |
| Unix     | Darwin | MacOS      | MacOS          | 9         | 9       | Mac OS 9              | Mac OS 9                                    |
| Unix     | Darwin | MacOSX     | Mac OS X       | 10.0      | 10.0.1  | Mac OS X 10.0         | Mac OS X 10.0.1                             |
| Unix     | Darwin | MacOSX     | OS X           | 10.8      | 10.8.3  | OS X 10.8             | OS X 10.8.3                                 |
| Unix     | Darwin | MacOSX     | macOS          | 10.12     | 10.12.4 | macOS 10.12           | macOS 10.12.4                               |
| Unix     | Darwin | MacOSX     | macOS          | 10.12     | 10.12.6 | macOS 10.12           | macOS 10.12.6                               |
| Unix     | Darwin | MacOSX     | macOS          | 10.13     | 10.13.6 | macOS 10.13           | macOS 10.13.4                               |
| Unix     | Darwin | MacOSX     | macOS          | 10.14     | 10.14.1 | macOS 10.14           | macOS 10.14.1                               |
| Unix     | Darwin | BSD        | FreeBSD        | 10        | 10.1    | FreBSD 10.1           | FreeBSD 10.1                                |
| Windows  | WinNT  | ReactOS    | ReactOS        | 5.2       | 5.2     | ReactOS 0.4           | ReactOS 0.4.9                               |
| Windows  | WinNT  | WindowsPE  | WinPE          | 2.0       | 6.0     | WinPE 2.0             | Windows (TM) Preinstallation Environment    |
| Windows  | WinNT  | Desktop    | Windows        | Vista     | 6.0     | Windows Vista         | Windows Vista                               |
| Windows  | WinNT  | Desktop    | Windows        | Vista     | 6.0     | Windows Vista         | Windows Vista	Service Pack 1                |
| Windows  | WinNT  | Desktop    | Windows        | Vista     | 6.0     | Windows Vista         | Windows Vista	Service Pack 2                |
| Windows  | WinNT  | Server     | Windows Server | 2008      | 6.0     | Windows Server 2008   | Windows Server 2008                         |
| Windows  | WinNT  | WindowsPE  | WinPE          | 3.0       | 6.1     | WinPE 3.0             | Windows (TM) Preinstallation Environment    |
| Windows  | WinNT  | Desktop    | Windows        | 7         | 6.1     | Windows 7             | Windows 7                                   |
| Windows  | WinNT  | Desktop    | Windows        | 7         | 6.1     | Windows 7             | Windows 7 Service Pack 1                    |
| Windows  | WinNT  | Server     | Windows Server | 2008r2    | 6.1     | Windows Server 2008r2 | Windows Server 2008 R2 Datacenter           |
| Windows  | WinNT  | Desktop    | Windows        | 8         | 6.2     | Windows 8             | Windows 8	Professsional                     |
| Windows  | WinNT  | Desktop    | Windows        | 8.1       | 6.3     | Windows 8.1           | Windows 8.1 Professsional                   |
| Windows  | WinNT  | Desktop    | Windows        | 8.1       | 6.3     | Windows 8.1           | Windows 8.1 Update Professsional            |
| Windows  | WinNT  | Server     | Windows Server | 2012r2    | 6.3     | Windows Server 2012r2 | Windows Server 2012 R2 Datacenter           |
| Windows  | WinNT  | Desktop    | Windows 10     | 1607      | 6.3     | Windows 10 1607       | Windows 10 Entreprise 2016 LTSB             |
| Windows  | WinNT  | Server     | Windows Server | 2016      | 6.3     | Windows Server 2016   | Windows Server 2016 Datacenter              |
| Windows  | WinNT  | ServerCore | Windows Server | 2016      | 6.3     | Windows Server 2016   | Windows Server Core 2016 Datacenter         |
| Windows  | WinNT  | WindowsPE  | WinPE	10    | 1703      | 6.3     | WinPE 10 1703         | Windows (TM) 10 Preinstallation Environment |
| Windows  | WinNT  | Desktop    | Windows 10     | 1803      | 6.3     | Windows 10 1803       | Windows 10 Professional                     |
| Windows  | WinNT  | Desktop    | Windows 10     | 1803      | 6.3     | Windows 10 1803       | Windows 10 Entreprise                       |
| Windows  | WinNT  | Desktop    | Windows 11     | 21H2      | 6.3     | Windows 11 21H2       | Windows 11 Entreprise                       |

> NOTE
>
> `$os.name` is simply a concatenation of `$os.distrib + $os.releaseId` if applicable.

### Create a new dictionary

As stated above, the dictionaries are loaded following this rule :

```code
$os.platform > $os.kernel > $os.family > $os.distrib > $os.ReleaseId
```

So, from the most generic to the most specific, the dictionnaries of an `Ubuntu
16.04` should be :

-   `Dict.Unix`
-   `Dict.Unix.Linux`
-   `Dict.Unix.Linux.Debian`
-   `Dict.Unix.Linux.Debian.Ubuntu`
-   `Dict.Unix.Linux.Debian.Ubuntu.1604`

Each dictionary should contain and use command that are relevant only to its
definition. That is, if it is necessary to use dpkg, it can be at the `Debian`
level.

The same way goes for Windows :

-   `Dict.Windows`
-   `Dict.Windows.WinNT`
-   `Dict.Windows.WinNT.Desktop`
-   `Dict.Windows.WinNT.Desktop.Windows10`
-   `Dict.Windows.WinNT.Desktop.Windows10.1803`

Or for a server :

-   `Dict.Windows`
-   `Dict.Windows.WinNT`
-   `Dict.Windows.WinNT.Server`
-   `Dict.Windows.WinNT.Server.WindowsServer`
-   `Dict.Windows.WinNT.Server.WindowsServer.2016`

Or even for Windows PE :

-   `Dict.Windows`
-   `Dict.Windows.WinNT`
-   `Dict.Windows.WinNT.WindowsPE`
-   `Dict.Windows.WinNT.WindowsPE.WinPE`
-   `Dict.Windows.WinNT.WindowsPE.WinPE.50`

We don't need everything to exist. The key is to put the right thing to the
right place.

So to create a new dictionary, we need to known which one we need to create.

### Simple example

Say we want to abstract the `shutdown` for all known OS's. We want to define a
new _cmdlet_ `Invoke-OSShutdown` without checking on wich OS the script is
running. We known that different OS's need different functions.

As far as I know, all unixes can use the command `shutdown -h now` (or
`poweroff` or `halt`). Windows desktop and server use `shutdown /s /t 1`, while
WinPE use `wpeutil shutdown`.

 what would be done :

-   `Dict.Unix` would contain

```powershell
function Invoke-OSShutdown {
    shutdown -h now
}
```

-   `Dict.Windows` would contain

```powershell
function Invoke-OSShutdown {
    shutdown /s /t 1
}
```

-   `Dict.Windows.WinNT.WindowsPE` would override previous definition with

```powershell
function Invoke-OSShutdown {
    wpeutil shutdown
}
```

## Special attributes

Special attributes are os's class member that need special attention.

-   `files` is a hashtable that reference well known files of the OS.
-   `folders` is a hashtable that reference well known folders of the OS.

### files

`files` is a hashtable that reference well known files of the OS. The key is the
role, or a nickname, and the value is the full absolute path to the file.

Example:

| file                 | OS                   | key    | value                               |
| -------------------- | -------------------- | ------ | ----------------------------------- |
| $os.files\['kernel'] | Ubuntu 16.04 LTS     | kernel | /boot/vmlinuz-4.4.0-98-generic      |
| $os.files\['kernel'] | Windows 10 LTSC 2019 | kernel | C:\\Windows\\System32\\ntoskrnl.exe |
| $os.files\['kernel'] | MacOS 10.14 Mojave   | kernel |                                     |

### folders

`folders` is a hashtable that reference well known folders of the OS. The key is
the role, or a nickname, and the value is the full absolute path to the folder.
It is advisable to add each folder twice or third time for each supported
Operating System as each system may reference same resource with differente
name.

look at the examples below :

| folders                     | OS                   | key         | value           |
| --------------------------- | -------------------- | ----------- | --------------- |
| $os.folders\['etc']         | Ubuntu 16.04 LTS     | etc         | /etc            |
| $os.folders\['etc']         | Windows 10 LTSC 2019 | etc         | C:\\ProgramData |
| $os.folders\['etc']         | MacOS 10.14 Mojave   | etc         | /private/etc    |
| $os.folders\['ProgramData'] | Ubuntu 16.04 LTS     | ProgramData | /etc            |
| $os.folders\['ProgramData'] | Windows 10 LTSC 2019 | ProgramData | C:\\ProgramData |
| $os.folders\['ProgramData'] | MacOS 10.14 Mojave   | ProgramData | /private/etc    |

This way, an admin/developer can use the name that fits the most reality for
him. If the script developed is target at windows only, he will use
`$os.folders['ProgrammData']`. If the script is cross-platform, he will either
use `$os.folders['etc']` or `$os.folders['ProgrammData']`
