# Tests

## Create OS distributions

### buster-x86

```shell
sudo root
debootstrap --arch=i386 --variant=minbase buster /mnt/buster-x86
```

### buster-x64

```shell
sudo root
debootstrap --arch=amd64 --variant=minbase buster /mnt/buster-x64
```
