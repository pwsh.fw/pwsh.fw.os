# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.Dict.OS.tests.ps1 -CodeCoverage ../PwSh.Fw.Dict.OS/PwSh.Fw.Dict.OS.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

# only run on windows
if (!$IsLinux) { exit }
Import-Module -FullyQualifiedName $ROOTDIR/PwSh.Fw.OS/Dictionaries/$ModuleName/$ModuleName.psm1 -Force -ErrorAction stop

Describe "$ModuleName" {

	Context "TaskScheduler" {
	
		# Mock Write-MyWarning { }
		# Mock ewarn { }
		# Mock Write-Host { } -ModuleName PwSh.Fw.Write

		It "Create a new Task Scheduler folder" {
			$rc = New-PwShFwScheduledTaskFolder -Name Test
			$rc | should -BeTrue

		}

		It "Create a new scheduled task #1" {
			$rc = New-PwShFwScheduledTask -Name "PwShFw.Test.task" -Description "Test task" -command "/bin/bash" -Parameters "-c 'echo This is a test' >> /pwshfw.test.txt" -Daily
			$rc | should -BeTrue
		}
	}
}
