# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.Platform.tests.ps1 -CodeCoverage ../PwSh.Fw.OS/Includes/PwSh.Fw.Platform.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

Import-Module -FullyQualifiedName $ROOTDIR/PwSh.Fw.OS/Includes/$ModuleName.psm1 -Force -ErrorAction stop

Describe "PwSh.Fw.Platform" {

	Context "Test-IsOSRoot" {

		It "Test-IsOSRoot -Path '$PSScriptRoot' return $false" {
			$rc = Test-IsOSRoot -Path '$PSScriptRoot'
			$rc | Should -BeFalse
		}

		It "Test-IsOSRoot -Path '$PSScriptRoot/buster-x86' return $true" {
			$rc = Test-IsOSRoot -Path "$PSScriptRoot/buster-x86"
			$rc | Should -BeTrue
		}

		It "Test-IsOSRoot -Path '$PSScriptRoot/buster-x64' return $true" {
			$rc = Test-IsOSRoot -Path "$PSScriptRoot/buster-x64"
			$rc | Should -BeTrue
		}

		It "Test-IsOSRoot -Path '$PSScriptRoot/winpe-x86' return $true" {
			$rc = Test-IsOSRoot -Path "$PSScriptRoot/winpe-x86"
			$rc | Should -BeTrue
		}

		It "Test-IsOSRoot -Path '$PSScriptRoot/winpe-x64' return $true" {
			$rc = Test-IsOSRoot -Path "$PSScriptRoot/winpe-x64"
			$rc | Should -BeTrue
		}

	}

	Context "Get-OSRoot" {

		if ($IsWindows) {

			It "[Windows] Get-OSRoot -Online return $env:SystemDrive" {
				$rc = Get-OSRoot -Online
				$rc | Should -BeExactly $env:SystemDrive
			}

			It "[Windows] Get-OSRoot -Path 'C:\Windows' return 'C:'" {
				$rc = Get-OSRoot -Path 'C:\Windows'
				$rc | Should -BeExactly 'C:'
			}
		}

		if ($IsLinux) {

			It "[Linux] Get-OSRoot -Online return '/'" {
				$rc = Get-OSRoot -Online
				$rc | Should -BeExactly '/'
			}

			It "[Linux] Get-OSRoot -Root '/var/lib/apt' return '/'" {
				$rc = Get-OSRoot -Path '/var/lib/apt'
				$rc | Should -BeExactly '/'
			}

		}

		if ($IsMacOS) {

			It "[macOS] Get-OSRoot -Online return '/'" {
				$rc = Get-OSRoot -Online
				$rc | Should -BeExactly '/'
			}

			It "[macOS] Get-OSRoot -Root '/Library' return '/'" {
				$rc = Get-OSRoot -Path '/Library'
				$rc | Should -BeExactly '/'
			}

		}

	}
}

Describe "PwSh.Fw.OS" {

	Context "PwSh.Fw.OS" {

		$os = Get-OperatingSystem -Online

		if ($IsLinux) {

			It "Get OS packages 'bash'" {
		
				$p = $os | Get-OSPackages -Name "bash"
				$p | Should -NotBe $null
			}

		}
	}
}
