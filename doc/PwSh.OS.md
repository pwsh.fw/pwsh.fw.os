# PwSh.OS Module
## Get-OnlineOperatingSystem
### Synopsis
Get informations about currently running Operating System
### Syntax
```powershell

Get-OnlineOperatingSystem [<CommonParameters>]




```
### Note
General notes

### Examples
**EXAMPLE 1**
```powershell
$os = Get-OnlineOpratingSystem
```


## Get-OperatingSystem
### Syntax
```powershell
Get-OperatingSystem -Root <string> [<CommonParameters>]

Get-OperatingSystem -Online [<CommonParameters>]
```
### Parameters
| Name  | Alias  | Description | Required? | Pipeline Input | Default Value |
| - | - | - | - | - | - |
| <nobr>Online</nobr> | None |  | true | true \(ByValue\) |  |
| <nobr>Root</nobr> | None |  | true | true \(ByValue\) |  |
## Get-OperatingSystemObject
### Synopsis
Get an operating system object.
### Syntax
```powershell

Get-OperatingSystemObject -Root <String> [-ShowMissing] [<CommonParameters>]

Get-OperatingSystemObject -Online [-ShowMissing] [<CommonParameters>]




```
### Parameters
| Name  | Alias  | Description | Required? | Pipeline Input | Default Value |
| - | - | - | - | - | - |
| <nobr>Root</nobr> |  |  | true | true \(ByValue\) |  |
| <nobr>Online</nobr> |  |  | true | true \(ByValue\) | False |
| <nobr>ShowMissing</nobr> |  |  | false | false | False |
### Note
General notes

## Load-Dictionaries
### Synopsis
Load online operating system's dictionaries.
### Syntax
```powershell

Load-Dictionaries [-Platform] <String> [-Kernel] <String> [-Family] <String> [-Distrib] <String> [-ReleaseId] <String> [-Force] [<CommonParameters>]




```
### Parameters
| Name  | Alias  | Description | Required? | Pipeline Input | Default Value |
| - | - | - | - | - | - |
| <nobr>Platform</nobr> |  | Platform os OS | true | true \(ByPropertyName\) |  |
| <nobr>Kernel</nobr> |  |  | true | true \(ByPropertyName\) |  |
| <nobr>Family</nobr> |  | Family of OS | true | true \(ByPropertyName\) |  |
| <nobr>Distrib</nobr> |  | Distribution name of OS | true | true \(ByPropertyName\) |  |
| <nobr>ReleaseId</nobr> |  | Release number of OS | true | true \(ByPropertyName\) |  |
| <nobr>Force</nobr> |  | Force to \(re-\)load module | false | false | False |
### Note
General notes

### Examples
**EXAMPLE 1**
```powershell

```
\`\`\`ps1
$os = Get-OperatingSystem -Online
$os | Load-Dictionaries
\`\`\`

