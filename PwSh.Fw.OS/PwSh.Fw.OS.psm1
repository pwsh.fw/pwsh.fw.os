$Script:NS = (get-item $PSCommandPath).basename

# function Get-PwShFwModuleInfos {
# 	[CmdletBinding()][OutputType([String])]Param (
# 		# [Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$string
# 	)
# 	Begin {
# 		Write-EnterFunction
# 	}

# 	Process {
# 		Write-Output "PSCommandPath = $PSCommandPath"
# 		Write-Output "PSScriptRoot = $PSScriptRoot"
# 	}

# 	End {
# 		Write-LeaveFunction
# 	}
# }

function New-OSObject {
	Begin {
	}

	Process {
		$os = New-Object -TypeName PSObject -Property @{
			platform = ""
			kernel = ""
			mainstream = ""
			kernelVersion = "0.0.0.0"
			family = ""
			distrib = ""
			edition = ""
			installType = ""
			version = ""
			displayVersion = ""
			releaseId = ""
			productName = ""
			codeName = ""
			longCodeName = ""
			name = ""
			arch = ""
			Online = $true

			# root folder of disk containing OS
			# '/' for online Unix/Linux systems
			# 'c:\' for online Windows systems
			Root = ""

			# root folder of OS on disk (from the OS point of view)
			# for online OS, $Root and $SystemRoot may be the same, especially unix and derivatives
			# for offline OS, $Root is the location of OS from the running OS, $SystemRoot is the location from the offline OS
			# '/' on Unix/Linux systems
			# 'c:\Windows' on Windows systems (can be C:\winnt on older systems)
			SystemRoot = ""

			files = @{}
			folders = @{}
			packages = [ordered]@{}
		}

		return $os
	}

	End {
	}
}

<#
.SYNOPSIS
Get an operating system object.

.DESCRIPTION
Returns an Operating System object.
An Operating System object is an object containing full identification of an Operating System.
That is its version, build number, codename, and so on.

.PARAMETER Online
True to specify to fetch information from the running OS

.PARAMETER Root
The path to the root of an OS.

.EXAMPLE
Get-OperatingSystem -Online

.EXAMPLE
Get-OperatingSystem -Root '/mnt/sda3'

.EXAMPLE
Get-OperatingSystem -Root F:\

.NOTES
General notes
#>
function Get-OperatingSystem {
    [CmdletBinding(DefaultParameterSetName="ROOT")]Param (
		[Parameter(ParameterSetName = "ROOT", Mandatory = $true, ValueFromPipeLine = $true)][string]$Root,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online
    )
	Begin {
		Write-EnterFunction
		$os = New-OSObject
	}

	Process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				$Root = Get-OSRoot -Online
			}
			'ROOT' {
				$Root = Get-OSRoot -Path $Root
			}
		}
		if ([string]::IsNullOrEmpty($Root)) { return $false }

		$os.Root = $Root
		$os.online = Test-OSIsOnline -Root $Root
		$os.platform = Get-OSPlatform -Root $Root
		$os.kernel = Get-OSKernel -Root $Root
		$os.mainstream = Get-OSMainstream -Kernel $os.kernel

		# From this point we need to load som librairies of OS to be discovered
		# $null = Load-Libraries -Platform $os.platform -Kernel $os.kernel
		# fill in attributes
		$attributes = @('family', 'distrib', 'kernelVersion', 'edition', 'version', 'releaseId', 'displayVersion', 'productName', 'codeName', 'longCodeName', 'name', 'arch', 'installType', 'files', 'folders', 'packages', 'systemRoot')
		foreach ($attribute in $attributes) {
			$os.$attribute = Get-OSAttribute -OS $os -Attribute $attribute
		}

		# From this point we need to load some dictionaries for currently running OS to go further
		if ($IsLinux) {
			$null = Load-Dictionaries -Platform "Unix" -Kernel "Linux"
		} elseif ($IsMacOS) {
			$null = Load-Dictionaries -Platform "Unix" -Kernel "Darwin"
		} elseif ($IsWindows) {
			$null = Load-Dictionaries -Platform "Windows" -Kernel "WinNT"
		}

		return $os
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Get an operating system object.

.DESCRIPTION
Returns an Operating System object.
An Operating System object is an object containing full identification of an Operating System.
That is its version, build number, codename, and so on.

.NOTES
General notes
#>
function Get-OperatingSystemObject {
    [CmdletBinding(DefaultParameterSetName="ROOT")]Param (
		[Parameter(ParameterSetName = "ROOT", Mandatory = $true, ValueFromPipeLine = $true)][string]$Root,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online,
		[switch]$ShowMissing
    )
    Begin {
		# eenter($MyInvocation.MyCommand)
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				if ($env:SystemDrive) {
					$Root = $env:SystemDrive
				} else {
					$Root = '/'
				}
			}
		}
		# edevel("PWSHFW_PATH = " + $Global:PWSHFW_PATH)
		$os = [ordered]@{}
		# ewarn "Get-OperatingSystemObject() use custom classes and custom object type."
		# ewarn "It may not work under certain circumstances."
		# ewarn "Windows 10 Pro and Scheduled Tasks are known to not work properly."
		# ewarn "Please migrate your script to use Get-OperatingSystem instead."
    }

    Process {
		return $null
    }

    End {
        # eleave($MyInvocation.MyCommand)
    }
}

<#
.SYNOPSIS
Get informations about currently running Operating System

.DESCRIPTION
Try to get concise yet useful and formatted informations about running OS. The way we get informations must run in constrained language mode.

.EXAMPLE
$os = Get-OnlineOpratingSystem

.NOTES
General notes
#>

function Get-OnlineOperatingSystem {
    # [CmdletBinding()]Param (
    # )
    Begin {
		Write-EnterFunction
		# $os = @{}
		$os = New-Object -TypeName PSObject -Property @{
			platform = "unknown"
			mainstream = "unknown"
			kernel = "unknown"
			kernelVersion = "0.0.0.0"
			family = "unknown"
			distrib = "unknown"
			edition = ""
			installType = "unknown"
			version = ""
			releaseId = ""
			productName = "unknown"
			codeName = "unknown"
			# longCodeName = "unknown"
			name = "unknown"
			arch = "x86"
			Online = $true
			Root = "/"
			SystemRoot = "/"
			# $files = @{}
			# $folders = @{}
			packages = [ordered]@{}
		}
    }

	Process {

		# handle 3 mainstream operating systems Windows - macOS - linux
		if ($IsLinux) {
			$os.platform = "Unix"
			$os.mainstream = "Linux"
			$os.kernel = "linux"
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot/Includes/OS.Unix/OS.Unix.psd1" -Force
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot/Includes/OS.Linux/OS.Linux.psd1" -Force
			$os.kernelVersion = (OS.Linux\Get-OSKernelVersion -Online)
			$os.family = (OS.Linux\Get-OSFamily -Online)
			$family = (get-culture).TextInfo.ToTitleCase($os.family) -replace '[\s\p{P}]'
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot/Includes/OS.$family/OS.$family.psd1" -Force -Policy Optional
			$os.distrib = (OS.Linux\Get-OSDistrib -Online)
			$distrib = (get-culture).TextInfo.ToTitleCase($os.distrib) -replace '[\s\p{P}]'
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot/Includes/OS.$distrib/OS.$distrib.psd1" -Force -Policy Optional
			$os.edition = (OS.Linux\Get-OSEdition -Online)
			$os.installType = (Get-OSInstalltype -Online)
			$os.version = (OS.Linux\Get-OSVersion -Online)
			$os.releaseId = (OS.Linux\Get-OSReleaseID -Online)
			$os.productName = (OS.Linux\Get-OSProductName -Online)
			$os.codeName = (OS.Linux\Get-OSCodeName -Online)
			$os.arch = (Get-OSArch -Online)
			$os.online = $true
			$os.root = '/'
			$os.systemRoot = '/'
			$os | Add-Member -NotePropertyName files -NotePropertyValue (OS.Linux\Get-OSFiles -Root $os.root)
			$os | Add-Member -NotePropertyName folders -NotePropertyValue (OS.Linux\Get-OSFolders -Root $os.root)
			$os.packages = (Get-OSPackages -Online)
		}

		if ($isMacos) {
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot\Includes\OS.Unix\OS.Unix.psd1" -Force
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot\Includes\OS.Macos\OS.Macos.psd1" -Force
			$os.platform = "Unix"
			$os.mainstream = "macOS"
			$os.kernel = "Darwin"
			$os.kernelVersion = (OS.Macos\Get-OSKernelVersion -Online)
			$os.distrib = "macOS"
			$osIdentity = OS.Macos\Get-OSIdentity -Online
			$os.family = "macOS"
			# $os.edition =
			$os.installType = "Desktop"
			$os.version = $osIdentity.ProductVersion
			$os.releaseId = OS.Macos\Get-OSReleaseId -version $os.version
			$os.codeName = OS.Macos\Get-OSCodeName -ReleaseId $os.releaseId
			$os.productName = $osIdentity.ProductName + " " + $os.version + " (" + $os.codename + ")"
			$os.arch = OS.Macos\Get-OSArch -Online
			$os.online = $true
			$os.root = '/'
			$os.systemRoot = '/'
			$os | Add-Member -NotePropertyName files -NotePropertyValue (OS.Macos\Get-OSFiles -Root $os.root)
			$os | Add-Member -NotePropertyName folders -NotePropertyValue (OS.Macos\Get-OSFolders -Root $os.root)
			$os.packages = (Get-OSPackages -Online)
		}

		if ($isWindows) {
			$null = Load-Module -fullyqualifiedName "$PSScriptRoot\Includes\OS.Windows\OS.Windows.psd1" -Force
			# $os.platform = "Windows"
			# $os.mainstream = "Windows"
			# $os.kernel = "WinNT"
			# $os.buildNumber = OS.Windows\Get-OSKernelVersion -Online
			# $os.distrib = OS.Windows\Get-OSDistrib -Online
			# $os.edition = OS.Windows\Get-OSEdition -Online
			# $os.installType = OS.Windows\Get-OSInstalltype -Online
			# $os.version = OS.Windows\Get-OSVersion -Online
			# $os.releaseId = OS.Windows\Get-OSReleaseID -Online
			# $os.productName = OS.Windows\Get-OSProductName -Online
			# $os.codeName = OS.Windows\Get-OSCodeName -Online
			# $os.arch = OS.Windows\Get-OSArch -Online
			# $os.online = $true
			# $os.root = $env:SystemDrive
			# $os.systemRoot = $env:SystemRoot
			# $os.files = OS.Windows\Get-OSFiles -Root $os.root
			# $os.folders = OS.Windows\Get-OSFolders -Root $os.root
			$os.platform = "Windows"
			$os.mainstream = "Windows"
			$os.kernel = "WinNT"
			$os.kernelVersion = (OS.Windows\Get-OSKernelVersion -Online)
			$os.version = (OS.Windows\Get-OSVersion -Online)
			$os.installType = (OS.Windows\Get-OSInstalltype -Online)
			$os.releaseId = (OS.Windows\Get-OSReleaseID -Online)
			$osIdentity = $os | Get-OSIdentity
			# for Windows before Windows 10
			if ([System.String]::IsNullOrEmpty($os.releaseId)) { $os.releaseId = $osIdentity.releaseId}
			# for Windows Server after Windows 2012r2
			if ($os.installType -eq 'Server') {
				if (-not([string]::IsNullOrEmpty($osIdentity.releaseId))) { $os.releaseId = $osIdentity.releaseId }
			}
			$os.family = $osIdentity.family
			$os.distrib = $osIdentity.distrib
			$os.codeName = $osIdentity.codename
			$os.edition = (OS.Windows\Get-OSEdition -Online)
			$os.productName = (OS.Windows\Get-OSProductName -Online)
			$os.arch = (OS.Windows\Get-OSArch -Online)
			$os.online = $true
			$os.root = ($env:SystemDrive)
			$os.systemRoot = ($env:SystemRoot)
			$os | Add-Member -NotePropertyName files -NotePropertyValue (OS.Windows\Get-OSFiles -Root $os.root)
			$os | Add-Member -NotePropertyName folders -NotePropertyValue (OS.Windows\Get-OSFolders -Root $os.root)
		}

		$os.name = ($os.distrib + ' ' + $os.releaseId)

		# return $os | Sort-HashTable
		return $os
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Load online operating system's dictionaries.

.DESCRIPTION
Dictionaries contain OS specific commands to do abstract OS layer from user's scripts.
This function load them according to a predefined hierarchy.

.PARAMETER Mainstream
Mainstream OS label (merely Windows / Macos / Linux)

.PARAMETER Platform
Platform os OS

.PARAMETER Family
Family of OS

.PARAMETER Distrib
Distribution name of OS

.PARAMETER ReleaseId
Release number of OS

.PARAMETER Path
Path from which to load dictionaries. Allow custom script to have its own dictionaries. Defaults to PwSh.Fw.OS module's dictionaries path

.PARAMETER Force
Force to (re-)load module

.EXAMPLE
Get-OperatingSystem -Online | Load-Dictionaries

.EXAMPLE
Load-Dictionaries -Platform Windows -Family Desktop -Distrib "Windows 10"

.EXAMPLE
Load-Dictionaries -Platform Linux -Family Debian -Distrib Ubuntu

.EXAMPLE
Load-Dictionaries -Platform MacOS

.NOTES
General notes
#>

function Load-Dictionaries {
	[CmdletBinding()]
	[OutputType([void])]
	[System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseApprovedVerbs", "", Justification="Load-Dictionaries is a more intuitive verb for this function.")]
	Param (
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Mainstream,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Platform,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Kernel,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Family,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Distrib,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$ReleaseId,
		[string]$Path = "$PSScriptRoot/Dictionaries",
		# Prefix to prepend to search for dictionaries
		[string]$Prefix,
		[Parameter()][switch]$Force
	)
	Begin {
		Write-EnterFunction
		# edevel("Platform = $Platform")
		# edevel("Family = $Family")
		# edevel("Distrib = $Distrib")
		# edevel("ReleaseId = $ReleaseId")
	}

	Process {
		# edevel("Platform = $Platform")
		# edevel("Kernel = $Kernel")
		# edevel("Family = $Family")
		# edevel("Distrib = $Distrib")
		# edevel("ReleaseId = $ReleaseId")
		$m = (get-culture).TextInfo.ToTitleCase($Mainstream) -replace '[\s\p{P}]'
		$p = (get-culture).TextInfo.ToTitleCase($platform) -replace '[\s\p{P}]'
		$k = (get-culture).TextInfo.ToTitleCase($Kernel) -replace '[\s\p{P}]'
		$f = (get-culture).TextInfo.ToTitleCase($Family) -replace '[\s\p{P}]'
		$d = (get-culture).TextInfo.ToTitleCase($Distrib) -replace '[\s\p{P}]'
		$r = (get-culture).TextInfo.ToTitleCase($ReleaseId) -replace '[\s\p{P}]'

		## Load PwSh Dictionaries
		# with fullyQualifiedName we are able to load Modules from devel folder
		# and I forgot that Dictionaries folder is not included in PsModulePath at install time
		if ($Prefix) { $dict = "$Prefix.Dict.OS" } else { $dict = "Dict.OS" }
		$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		if ($null -ne $Mainstream) {
			if ($Prefix) { $dict = "$Prefix.Dict.$m" } else { $dict = "Dict.$m" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}
		if ($null -ne $Platform) {
			if ($Prefix) { $dict = "$Prefix.Dict.$p" } else { $dict = "Dict.$p" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}
		if ($null -ne $Kernel) {
			if ($Prefix) { $dict = "$Prefix.Dict.$p.$k" } else { $dict = "Dict.$p.$k" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}
		if ($null -ne $Family) {
			if ($Prefix) { $dict = "$Prefix.Dict.$p.$k.$f" } else { $dict = "Dict.$p.$k.$f" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}
		if ($null -ne $Distrib) {
			if ($Prefix) { $dict = "$Prefix.Dict.$p.$k.$f.$d" } else { $dict = "Dict.$p.$k.$f.$d" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}
		if ($null -ne $ReleaseId) {
			if ($Prefix) { $dict = "$Prefix.Dict.$p.$k.$f.$d.$r" } else { $dict = "Dict.$p.$k.$f.$d.$r" }
			$null = Load-Module -Policy Optional -FullyQualifiedName "$Path/$dict/$dict.psd1" -Force:$Force
		}

		# ## Load User's script Dictionaries
		# if ($null -ne $Platform) {
		# 	$dict = "Dict.$p"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Dictionaries/$dict/$dict.psd1") -Force:$Force
		# }
		# if ($null -ne $Kernel) {
		# 	$dict = "Dict.$k"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Dictionaries/$dict/$dict.psd1") -Force:$Force
		# }
		# if ($null -ne $Distrib) {
		# 	$dict = "Dict.$d"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Dictionaries/$dict/$dict.psd1") -Force:$Force
		# }

	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Load offline operating system's Libraries.

.DESCRIPTION
Libraries contain specific knowledge to related online or offline OS.
This function load them according to a predefined hierarchy.

.PARAMETER Platform
Platform os OS

.PARAMETER Family
Family of OS

.PARAMETER Distrib
Distribution name of OS

.PARAMETER ReleaseId
Release number of OS

.PARAMETER Force
Force to (re-)load module

.EXAMPLE
Load-Libraries -Platform Windows -Family Desktop -Distrib "Windows 10"

.EXAMPLE
Load-Libraries -Platform Linux -Family Debian -Distrib Ubuntu

.EXAMPLE
Load-Libraries -Platform MacOS

.NOTES
General notes
#>

function Load-Libraries {
	[CmdletBinding()]
	[System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseApprovedVerbs", "", Justification="Load-Libraries is a more intuitive verb for this function.")]
	Param (
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Platform,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Kernel,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Family,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$Distrib,
		[AllowNull()][AllowEmptyString()][Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)][string]$ReleaseId,
		[Parameter()][switch]$Force
	)
	Begin {
		Write-EnterFunction
		# edevel("Platform = $Platform")
		# edevel("Family = $Family")
		# edevel("Distrib = $Distrib")
		# edevel("ReleaseId = $ReleaseId")
	}

	Process {
		# edevel("Platform = $Platform")
		# edevel("Kernel = $Kernel")
		# edevel("Family = $Family")
		# edevel("Distrib = $Distrib")
		# edevel("ReleaseId = $ReleaseId")
		$p = (get-culture).TextInfo.ToTitleCase($platform) -replace '[\s\p{P}]'
		$k = (get-culture).TextInfo.ToTitleCase($Kernel) -replace '[\s\p{P}]'
		$f = (get-culture).TextInfo.ToTitleCase($Family) -replace '[\s\p{P}]'
		$d = (get-culture).TextInfo.ToTitleCase($Distrib) -replace '[\s\p{P}]'
		$r = (get-culture).TextInfo.ToTitleCase($ReleaseId) -replace '[\s\p{P}]'

		## Load PwSh Libraries
		# with fullyQualifiedName we are able to load Modules from devel folder
		# and I forgot that Libraries folder is not included in PsModulePath at install time
		$dict = "OS"
		$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		if ($null -ne $Platform) {
			$dict = "OS.$p"
			$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		}
		if ($null -ne $Kernel) {
			$dict = "OS.$k"
			$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		}
		if ($null -ne $Family) {
			$dict = "OS.$f"
			$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		}
		if ($null -ne $Distrib) {
			$dict = "OS.$d"
			$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		}
		if ($null -ne $ReleaseId) {
			$dict = "OS.$d.$r"
			$null = Load-Module -Policy Optional -FullyQualifiedName "$PSScriptRoot/Libraries/$dict/$dict.psd1" -Force:$Force -Quiet:(-not $Global:TRACE)
		}

		# ## Load User's script Libraries
		# if ($null -ne $Platform) {
		# 	$dict = "OS.$p"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Libraries/$dict/$dict.psd1") -Force:$Force -Quiet
		# }
		# if ($null -ne $Kernel) {
		# 	$dict = "OS.$k"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Libraries/$dict/$dict.psd1") -Force:$Force -Quiet
		# }
		# if ($null -ne $Distrib) {
		# 	$dict = "OS.$d"
		# 	$null = Load-Module -Policy Optional -FullyQualifiedName $($Global:DIRNAME + "/Libraries/$dict/$dict.psd1") -Force:$Force -Quiet
		# }

	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Retrive the attribute of an Operating System.

.DESCRIPTION
Get the attribute of an Operating System using what have been found so far. It uses OS Libraries to discover correct values

.PARAMETER OS
a partial OS object

.PARAMETER Attribute
Name of attribute of OS object to fetch

.EXAMPLE
Get-OSAttribute -OS $os -Attribute "version"

.NOTES
General notes
#>

function Get-OSAttribute {
	[CmdletBinding()][OutputType([Object])]Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][Object]$OS,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][String]$Attribute
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		# search function from the more specific to the most generic
		edevel "Going to fetch '$Attribute' attribute"
		$searchOrder = @('distrib', 'family', 'kernel', 'platform')
		$os | Load-Libraries
		$Attribute = (get-culture).TextInfo.ToTitleCase($Attribute) -replace '[\s\p{P}]'
		foreach ($s in $searchOrder) {
			$Value = $null
			# ns: name space. It is the namespace to search.
			# if $s is kernel, we want to search in $os.$s (OS.WinNT), but we need to work on value of $os.$s to be usable
			# for example, $s is distrib with a value of "Windows10", we need to search in "OS.Windows10" and not "OS.Windows 10"
			$ns = (get-culture).TextInfo.ToTitleCase($os.$s) -replace '[\s\p{P}]'
			try {
				# edebug "try to load OS.$ns\Get-OS$Attribute function"
				$null = Get-Command -Name "OS.$ns\Get-OS$Attribute" -Module "OS.$ns" -ErrorAction SilentlyContinue
				if ($?) {
					if ($os.Online) {
						$Value = invoke-expression "OS.$ns\Get-OS$Attribute -Online"
					} else {
						$Value = invoke-expression "OS.$ns\Get-OS$Attribute -Root $($os.Root)"
					}
					if ($Value) { break }
				} else {
					if ($Global:TRACE) { ewarn "OS.$ns\Get-OS$Attribute is not defined." }
					$Value = $null
				}
			} catch {
				if ($Global:TRACE) { eerror "An error occured while playing with 'OS.$ns\Get-OS$Attribute -Online:$($os.online) -Root $($os.Root)'" }
				$Value = $null
			}
		}
		return $Value
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSPackage {
	[CmdletBinding(DefaultParameterSetName='byPackageName')]Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)]$InputObject,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $false, ParameterSetName = 'byPackageName')][string]$Name,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $false, ParameterSetName = 'byFileName')][string]$Filename
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		switch ($PSCmdlet.ParameterSetName) {
			'byPackageName' {
				# $oPackage = $InputObject.packages.'$name'
				# break
			}
			'byFileName' {
				$name = Get-OSPackagenameFromFilename -Online:$InputObject.Online -Root $InputObject.Root -Filename $Filename
			}
		}

		if ($null -ne $name) {
			return $InputObject.packages."$name"
		} else {
			return $null
		}
	}

	End {
		Write-LeaveFunction
	}
}

