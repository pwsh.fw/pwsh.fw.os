$Script:NS = (get-item $PSCommandPath).basename

<#
.SYNOPSIS
Get Debian-based OS architecture

.DESCRIPTION


.PARAMETER Online
Specify you want to get the archtiecture of the running OS. It is equivalent of specifying -Root /

.PARAMETER Root
Specify to inquire the OS located at the specified mountpoint

.EXAMPLE
Get-OSArch -Online

.EXAMPLE
Get-OSArch -Root /mnt/sda3

.NOTES

#>

function Get-OSArch {
	[CmdletBinding()][OutputType([String])]Param (
		[switch]$Online,
        [string]$Root = "/"
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		if ($Online) { $Root = '/' }

		if (fileExist "$Root/var/lib/dpkg/arch") {
			$x86 = Get-Content "$Root/var/lib/dpkg/arch" | Select-String "^i386$"
			$x64 = Get-Content "$Root/var/lib/dpkg/arch" | Select-String "^amd64$"
			$arm32 = Get-Content "$Root/var/lib/dpkg/arch" | Select-String "^armhf$"
			$arm64 = Get-Content "$Root/var/lib/dpkg/arch" | Select-String "^arm64$"
		} elseif (fileExist "$Root/var/lib/dpkg/status") {
			$x86 = Get-Content "$Root/var/lib/dpkg/status" | Select-String "^Architecture: i386" | Select-Object -First 1
			$x64 = Get-Content "$Root/var/lib/dpkg/status" | Select-String "^Architecture: amd64" | Select-Object -First 1
			$arm32 = Get-Content "$Root/var/lib/dpkg/status" | Select-String "^Architecture: armhf" | Select-Object -First 1
			$arm64 = Get-Content "$Root/var/lib/dpkg/status" | Select-String "^Architecture: arm64" | Select-Object -First 1
		}

		if ($x86) {	$arch = "x86" }
		if ($x64) {	$arch = "x64" }
		if ($arm32) {	$arch = "arm32" }
		if ($arm64) {	$arch = "arm64" }

		return $arch
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSPackages {
    [CmdletBinding()][OutputType([Hashtable])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
	Begin {
		Write-EnterFunction
	}

	Process {
		if ($Online) { $Root = '' }
		$File = "$Root/var/lib/dpkg/status"
		# $aPackages = @()
		$hPackages = @{}
		# get a raw string (not an array of strings)
		$status = Get-Content $File -Raw
		# extract each paragraph to a single match
		$aStatus = $status | select-string -pattern '(?sm)^Package: .*?^$' -AllMatches
		ForEach ($m in $aStatus.Matches) {
			# extract paragraphs one-by-one
			$sPackage = $m.Value
			# escape offending \[a-z] characters
			$sPackage = $sPackage -replace '(?sm)\\', '\\\\'
			# convert multilines to a single line with '\n' chars
			$sPackage = $sPackage -replace '(?sm)\r?\n ', '\n '
			# convert "key: value" to "key = value"
			$sPackage = $sPackage -replace '(?sm)^([A-Z].*?):', '$1 ='
			# convert to data
			$oPackage = $sPackage | ConvertFrom-StringData
			# add to hastable
			if (-not($hPackages.Contains($oPackage.Package))) {
				$hPackages.Add($oPackage.Package, $oPackage)
			}
		}

		return ($hPackages | Sort-HashTable)
		# return $hPackages
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSPackagenameFromFilename {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
		[string]$Root = "/",
		[string]$Filename
    )
	Begin {
		Write-EnterFunction
	}

	Process {
		if ($Online) { $Root = '' }
		$infoDir = "$Root/var/lib/dpkg/info"
		$found = Select-String -Path $($infoDir + "/*.list") -Pattern $("^" + $Filename + "$") | Select-Object Path
		if ($found) {
			$dpkgInfoFile = Get-Item ($found).Path
			return $dpkgInfoFile.BaseName
		} else {
			return $null
		}
	}

	End {
		Write-LeaveFunction
	}
}
