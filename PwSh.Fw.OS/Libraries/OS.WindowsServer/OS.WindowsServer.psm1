<#
.SYNOPSIS
Get the release ID of an OS

.DESCRIPTION
Windows Server (since Windows 10 / 2016) use the same ReleaseId as Windows Client.
But to differentiate Servers from Clients, we will use the old "year" number like "2008", "2012r2"

.PARAMETER Online
True to specify to fetch information from the running OS

.PARAMETER Root
The path to the root of an OS.

.EXAMPLE
Get-OSReleaseId -Online

.EXAMPLE
Get-OSReleaseId -Root F:\

.NOTES
General notes
#>

function Get-OSReleaseId {
    [CmdletBinding()][OutputType([String])]Param (
		[Parameter(ParameterSetName = 'ONLINE')][switch]$Online,
		[Parameter(ParameterSetName = 'ROOT')][string]$Root
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				$CurrentVersion = Get-OSVersion -Online
				$CurrentBuild = Get-OSKernelVersion -Online
				$releaseId = OS.WinNT\Get-OSReleaseId -Online
				break
			}
			'ROOT' {
				$CurrentVersion = Get-OSVersion -Root $Root
				$CurrentBuild = Get-OSKernelVersion -Root $Root
				$releaseId = OS.WinNT\Get-OSReleaseId -Root $Root
				break
			}
		}

		switch ($CurrentVersion) {
			"5.0" {
				$releaseId = "2000"
			}
			"5.1" {
				$releaseId = "2003"
			}
			"5.2" {
				$releaseId = "2003r2"
			}
			"6.0" {
				switch ($CurrentBuild) {
					"6000" {
						$releaseId = "2008"
						break
					}
					"6001" {
						$releaseId = "2008sp1"
						break
					}
					"6002" {
						$releaseId = "2008sp2"
						break
					}
				}
			}
			"6.1" {
				switch ($CurrentBuild) {
					"7600" {
						$releaseId = "2008r2"
						break
					}
					"7001" {
						$releaseId = "2008r2sp1"
						break
					}
				}
			}
			"6.2" {
				switch ($CurrentBuild) {
					"9200" {
						$releaseId = "2012"
						break
					}
				}
			}
			"6.3" {
				switch ($CurrentBuild) {
					# * Windows 8
					"9300" {
						$releaseId = "2012r2"
						break
					}
					# * Windows 8.1
					"9600" {
						$releaseId = "2012r2sp1"
						break
					}
					# * Windows 10
					# "10240", etc...
					default {
						switch ($ReleaseId) {
							'1607' {
								$releaseId = "2016"
							}
							'1809' {
								$releaseId = "2019"
							}
							'2009' {
								$releaseId = "2022"
							}
						}
						break
					}
				}
			}
		}

		# return $Distrib + "." + $ReleaseId.replace('.','')
		return "$ReleaseId"
   }

    End {
        Write-LeaveFunction
    }
}

