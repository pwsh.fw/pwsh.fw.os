<#
#>

$Script:NS = (get-item $PSCommandPath).basename

# force english language to prevent weird localized output
$env:LANG = "en_US.UTF-8"
$env:LANGUAGE = "en_US"

<#
.SYNOPSIS
Return a the commonly used name for an OS.

.DESCRIPTION
Return a the commonly used name for an OS.

.PARAMETER Online
True to specify to fetch information from the running OS

.PARAMETER Root
The path to the root of an OS.

.EXAMPLE
Get-OSName -Online

.EXAMPLE
Get-OSName -Root '/mnt/sda3'

.EXAMPLE
Get-OSName -Root 'F:\'

.NOTES
General notes
#>
function Get-OSName {
    [CmdletBinding()][OutputType([String])]Param (
		[Parameter(ParameterSetName = 'ONLINE')][switch]$Online,
		[Parameter(ParameterSetName = 'ROOT')][string]$Root
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		if ($Online) { $Root = '/' }
		$distrib = Get-OSDistrib -Root $Root
		$releaseId = Get-OSReleaseId -Root $Root
		# return $Distrib + "." + $ReleaseId.replace('.','')
		return "$Distrib $ReleaseId"
   }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS
Simple ping to a destination on the network

.DESCRIPTION
Long description

.PARAMETER NameOrIP
Name or IPAddress of destination host to ping

.PARAMETER count
Optional number of echo request to send. Default to 1.

.EXAMPLE
Invoke-Ping -NameOrIP localhost

.EXAMPLE
Invoke-Ping -NameOrIP remote.server.domain.com -Count 1

#>
function Invoke-Ping {
    [CmdletBinding()]Param (
        [string]$NameOrIP = "localhost",
        [int]$count = 1,
		[int]$timeout = 1
    )
    Begin {
        Write-EnterFunction
    }

    Process {
        ping -c $count -W $timeout $NameOrIP
        return $?
   }

    End {
        Write-LeaveFunction
    }
}
