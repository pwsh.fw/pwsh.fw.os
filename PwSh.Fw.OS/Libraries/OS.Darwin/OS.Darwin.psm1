<#
	.SYNOPSIS
	Get the family of the OS
#>
function Get-OSFamily {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
        return "macOS"
   }

    End {
        Write-LeaveFunction
    }
}

