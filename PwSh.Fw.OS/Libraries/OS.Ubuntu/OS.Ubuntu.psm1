$Script:NS = (get-item $PSCommandPath).basename

<#
	.SYNOPSIS
	Get the installation type of the OS

	.DESCRIPTION
	The installation type of an OS can be
	. Server
	. Desktop
	. PreOS
#>
function Get-OSInstallType {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        # eenter($Script:NS + "\" + $MyInvocation.MyCommand)
    }

    Process {
		$installType = "Desktop"
		if ($Online) { $Root = '' }

		$null = Get-Content "$Root/var/lib/dpkg/status" -Raw | Select-String "^Package: ubuntu-desktop" | Select-Object -First 1
		if ($ubuntuDesktop) {
			$installType = "Desktop"
		}
		$null = Get-Content "$Root/var/lib/dpkg/status" -Raw | Select-String "^Package: ubuntu-server" | Select-Object -First 1
		if ($ubuntuServer) {
			$installType = "Server"
		}

		return $installType
   }

    End {
        # eleave($Script:NS + "\" + $MyInvocation.MyCommand)
    }
}

