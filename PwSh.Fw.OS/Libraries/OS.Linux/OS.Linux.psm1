<#
#>

$Script:NS = (get-item $PSCommandPath).basename

function Get-OSKernelVersion {
	[CmdletBinding()][OutputType([String])]Param (
		[switch]$Online,
        [string]$Root = "/"
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		if ($Online) { $Root = '/' }
		$matches = (Get-ChildItem $Root/boot/).name | Select-String -Pattern "-(?<version>\d{1,}\.\d{1,}\.\d{1,}-\d{1,})-"
		$versions = ($matches.Matches.Groups | Where-Object { $_.Name -eq "version" }).Value | Sort-Object -Unique
		$matches2 = $versions | Select-String -Pattern "(?<major>\d{1,})\.(?<minor>\d{1,})\.(?<build>\d{1,})-(?<rev>\d{1,})"
		$versions2 = @()
		$matches2 | ForEach-Object {
			$versions2 += [System.Version]"$(($_.Matches.Groups | where-Object { $_.name -eq "major" }).Value).$(($_.Matches.Groups | where-Object { $_.name -eq "minor" }).Value).$(($_.Matches.Groups | where-Object { $_.name -eq "build" }).Value).$(($_.Matches.Groups | where-Object { $_.name -eq "rev" }).Value)"
		}
		return ($versions2 | Sort-Object | Select-Object -Last 1)
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSFamily {
	[CmdletBinding()][OutputType([String])]Param (
		[switch]$Online,
        [string]$Root = "/"
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		if ($Online) { $Root = '/' }
		$family = ""
		if (fileExist "$Root/etc/os-release") {
			$OSRELEASE = Get-Content "$Root/etc/os-release" -Raw | ConvertFrom-StringData
			if ($OSRELEASE.ID_LIKE) {
				$family = $OSRELEASE.ID_LIKE.Tolower()
			} elseif ($OSRELEASE.ID) {
				$family = $OSRELEASE.ID.Tolower()
			}
		}
		if (fileExist "$Root/etc/os-release") { $family = "debian" }
		return (get-culture).TextInfo.ToTitleCase($family)
	}

	End {
		Write-LeaveFunction
	}
}

<#
	.SYNOPSIS
	Get the distribution of the OS
#>
function Get-OSDistrib {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
            # $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
            $id = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "ID"
            $c = (Get-Culture).TextInfo
			$distrib = $c.ToTitleCase($id)
        }

		return $distrib
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the distribution of the OS
#>

<#
.SYNOPSIS
Get the OS codename

.DESCRIPTION
The codename is often used to quicky identify an OS in a unique and friendly way.

.PARAMETER Online
Specify to look at the currently running OS

.PARAMETER Root
The root path under wich to look for the OS

.EXAMPLE
Get-OSCodename -Online

.NOTES
General notes
#>

function Get-OSCodename {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = $null
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
			$value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION_CODENAME"
			# /etc/os-release sometimes lacks the VERSION_CODENAME property... try to find something else
			if ([string]::IsNullOrEmpty($value)) {
				$value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION"
				# find something between parenthesis
				$value = $value -replace ".*\((.*)\)", '$1'			}
		}

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the distribution of the OS
#>
function Get-OSProductname {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = ""
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
			$value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "PRETTY_NAME"
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the distribution of the OS
#>
function Get-OSVersion {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = ""
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
            $value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION"
            # remove everything that is not a digit, not a dot
            $value = $value -replace "[^0-9.]", ""
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the distribution of the OS
#>
function Get-OSDisplayVersion {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = ""
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
            $value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION"
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS


.DESCRIPTION
Long description

.PARAMETER Online
Parameter description

.PARAMETER Path
Parameter description

.EXAMPLE
An example

.NOTES
General notes
#>
function Get-OSReleaseId {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = ""
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
            $value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION_ID"
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the Product name of the OS
#>
function Get-OSEdition {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
        $value = ""
        if ($Online) {
            $value = $env:XDG_CURRENT_DESKTOP
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

<#
	.SYNOPSIS
	Get the Product name of the OS
#>
function Get-OSLongCodeName {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root = "/"
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		$value = ""
        $fileExist = Test-Path -Path "$Root/etc/os-release" -PathType Leaf
        if ($fileExist) {
			# $distrib = (Get-Content "$Root/etc/os-release" | where { $_ -match "^NAME=" }).split("=")[1].replace('"','')
			$value = Get-PropertyValueFromFile -Filename "$Root/etc/os-release" -Propertyname "VERSION"
			$value = $value.Split("(")[-1].Replace(")", "")
        }

		return $value
   }

    End {
        Write-LeaveFunction
    }
}

function Get-OSFiles {
	[CmdletBinding()][OutputType([Hashtable])]Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Root
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		$Root = $Root.Trim('/')
		$wkFiles = @{}
		# if (FileExist ("$Root/sbin\ntoskrnl.exe")) {
		# 	$wkFiles.Add('kernel', "$Root/sbin\ntoskrnl.exe")
		# }
		return ($wkFiles | Sort-HashTable)
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSFolders {
	[CmdletBinding()][OutputType([Hashtable])]Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Root
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		$Root = $Root.Trim('/')
		$wkFolders = @{}
		if (DirExist ("$Root/etc")) {
			$wkFolders.Add('etc', "$Root/etc")
			# $wkFolders.Add('ProgramData', "$Root/etc")
		}
		if (DirExist ("$Root/bin")) {
			$wkFolders.Add('bin', "$Root/bin")
			$wkFolders.Add('bin-x86', "$Root/bin")
			$wkFolders.Add('bin-x64', "$Root/bin")
			$wkFolders.Add('ProgramFiles', "$Root/bin")
			$wkFolders.Add('ProgramFiles-x86', "$Root/bin")
			$wkFolders.Add('ProgramFiles-x64', "$Root/bin")
		}
		if (DirExist ("$Root/sbin")) {
			$wkFolders.Add('sbin', "$Root/sbin")
			$wkFolders.Add('sbin-x64', "$Root/sbin")
			$wkFolders.Add('System-x86', "$Root/sbin")
			$wkFolders.Add('System-x64', "$Root/sbin")
		}
		if (DirExist ("$Root/var/lib")) {
			$wkFolders.Add('var', "$Root/var/lib")
			$wkFolders.Add('ProgramData', "$Root/var/lib")
		}
		if (DirExist ("$Root/var/log")) {
			$wkFolders.Add('log', "$Root/var/log")
		}
		return ($wkFolders | Sort-HashTable)
	}

	End {
		Write-LeaveFunction
	}
}

function Get-OSSystemRoot {
    [CmdletBinding()][OutputType([String])]Param (
        [switch]$Online,
        [string]$Root
    )
    Begin {
        Write-EnterFunction
    }

    Process {
		return "/"
   }

    End {
        Write-LeaveFunction
    }
}

