<#

  ######   #######  ##     ## ########  ##     ## ######## ######## ########
 ##    ## ##     ## ###   ### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## #### #### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## ## ### ## ########  ##     ##    ##    ######   ########
 ##       ##     ## ##     ## ##        ##     ##    ##    ##       ##   ##
 ##    ## ##     ## ##     ## ##        ##     ##    ##    ##       ##    ##
  ######   #######  ##     ## ##         #######     ##    ######## ##     ##

#>

<#
.SYNOPSIS
Know if computer was booted using old legacy BIOS mode or new UEFI.

.DESCRIPTION
To make next boot efficient, we need to know in what mode the computer booted to this stage.
This function will tell us that.
If every tests fail, or if we can't determined the boot mode, it default to "BIOS" mode.

.EXAMPLE
$bootMode = Get-ComputerBootMode

.NOTES
General notes
#>
function Get-ComputerBootMode {
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		# [Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$string
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		switch ($env:firmware_type) {
			'Legacy' {
				return "BIOS"
			}
			'UEFI' {
				return "UEFI"
			}
		}

		if ($null = Test-RegValueExist -RegPath 'HKLM:\System\CurrentControlSet\Control' -Name PEFirmwareType) {
			$value = Get-ItemPropertyValue 'HKLM:\System\CurrentControlSet\Control' -Name PEFirmwareType
			switch ($value) {
				1		{ return "BIOS" }
				2		{ return "UEFI" }
				default { return "BIOS" }
			}
		}

		return "BIOS"
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Configure computer to boot on user-asked partition

.DESCRIPTION
This function configure the boot flag on the correct partition

.PARAMETER Force
Force things

.PARAMETER Label
Label of the partition to configure

.PARAMETER MountPoint
Mountpoint of the target partition

.PARAMETER Disk
Disk device name of the partition

.PARAMETER PartNum
Partition number (can be several strings char since nvme disks. e.g. 'p1')

.PARAMETER Device
Full device adress like in '/dev/sda1'

.NOTES
General notes
#>
function Set-ComputerNextBootBIOS {
	[CmdletBinding()]
	[OutputType([Boolean])]
	Param (
		[switch]$Force,
		[Parameter(Mandatory = $true, ParameterSetName = 'LABEL')][string]$Label,
		[Alias('Letter', 'DriveLetter')]
		[Parameter(Mandatory = $true, ParameterSetName = 'MOUNTPOINT')][string]$MountPoint,
		[AllowNull]
		[Parameter(Mandatory = $false, ParameterSetName = 'DISKPART')][string]$Disk,
		[Parameter(Mandatory = $true, ParameterSetName = 'DISKPART')][string]$PartNum,
		[Parameter(Mandatory = $true, ParameterSetName = 'DEVICE')][string]$Device
		)
	Begin {
		Write-EnterFunction
	}

	Process {
		$diskNumber = -1
		$partNumber = -1
		switch ($PSCmdlet.ParameterSetName) {
			'LABEL' {
				$volume = Get-Volume -FileSystemLabel $Label
				$part = Get-Partition -Volume $volume
				$diskNumber = $part.DiskNumber
				$partNumber = $part.PartitionNumber
			}
			'MOUNTPOINT' {
				# we use $MountPoint[0] to get only the (1st) letter, stripping out the ':'
				$part = Get-Partition -DriveLetter $MountPoint[0]
				$diskNumber = $part.DiskNumber
				$partNumber = $part.PartitionNumber
			}
			'DISKPART' {
				$diskNumber = $Disk
				$partNumber = $PartNum
			}
			'Device' {
				eerror "-Device not supported on Windows."
			}
		}
		if ($diskNumber -lt 0) {
			eerror "Disk not found."
			return $false
		}
		if ($partNumber -lt 1) {
			eerror "Partition not found."
			return $false
		}

# 		$script = @"
# select disk ${diskNumber}
# select part ${partNumber}
# active
# "@
# 		$script | Out-File -FilePath "$TMP\nextboot.diskpart"
# 		Get-Content -Raw "$TMP\nextboot.diskpart" | ForEach-Object { edevel "$_" }
# 		$rc = Execute-Command -exe diskpart -args "/s $TMP\nextboot.diskpart"

		Set-Partition -DiskNumber $diskNumber -PartitionNumber $partNumber -IsActive $true

		return $rc
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Configure computer to boot on user-asked partition

.DESCRIPTION
This function configure UEFI to next-boot a specified device

.NOTES
General notes
#>
function Set-ComputerNextBootUEFI {
	[CmdletBinding()]
	[OutputType([Boolean])]
	Param (
		# [switch]$Force,
		# Label of an UEFI boot entry
		[Parameter(Mandatory = $false)][string]$UefiLabel = "Klonebot Boot Manager",
		# Label of a partition
		[Alias('Label', 'PartitionLabel')]
		[Parameter(Mandatory = $false, ParameterSetName = 'PARTLABEL')][string]$PartLabel,
		# MountPoint of a currently mounted volume or partition
		[Alias('Volume', 'Letter', 'DriveLetter')]
		[Parameter(Mandatory = $false, ParameterSetName = 'MOUNTPOINT')][string]$MountPoint,
		# disk device name
		[AllowNull]
		[Parameter(Mandatory = $false, ParameterSetName = 'DISKPART')][string]$Disk,
		# partition number
		[Parameter(Mandatory = $false, ParameterSetName = 'DISKPART')][string]$PartNum,
		# full device address
		[Parameter(Mandatory = $false, ParameterSetName = 'DEVICE')][string]$Device,
		# Boot PXE using IPv4 stack (only available with UEFI firmware)
		[Alias('PXE')]
		[Parameter(Mandatory = $false, ParameterSetName = 'NETWORK')][switch]$PXEv4,
		# Boot PXE using IPv6 stack (only available with UEFI firmware)
		[Parameter(Mandatory = $false, ParameterSetName = 'NETWORK')][switch]$PXEv6
		)
	Begin {
		Write-EnterFunction
	}

	Process {
		$entry = $null
		switch ($PSCmdlet.ParameterSetName) {
			'MOUNTPOINT' {
				$entry = Get-UEFIBootEntry -Key "osdevice" -Value "partition=$MountPoint"
			}
			'DISKPART' {
			}
			'Device' {
				$entry = Get-UEFIBootEntry -Key "device" -Value "$Device"
			}
			'NETWORK' {
				# try a set of values as manufacturer use their own
				if ($PXEv4) {
					# Dell
					if ([string]::IsNullOrEmpty($entry)) {
						$entry = Get-UEFIBootEntry -UefiLabel "Onboard NIC(IPV4)"
					}
				}
				if ($PXEv6) {
					# Dell
					if ([string]::IsNullOrEmpty($entry)) {
						$entry = Get-UEFIBootEntry -UefiLabel "Onboard NIC(IPV6)"
					}
				}
				# Dell
				if ([string]::IsNullOrEmpty($entry)) {
					$entry = Get-UEFIBootEntry -UefiLabel "Onboard NIC"
				}
				# VMware
				if ([string]::IsNullOrEmpty($entry)) {
					$entry = Get-UEFIBootEntry -UefiLabel "EFI Network"
				}
			}
		}

		if ([string]::IsNullOrEmpty($entry)) { return $false }
		$rc = Execute-Command -exe $Script:bcdedit -args "/bootsequence $($entry.id)"
		return $rc
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Get details of an UEFI boot entry

.DESCRIPTION
Return UEFI boot entry details as an object

.EXAMPLE
Get-UEFIBootEntry -UefiLabel "Windows Boot Manager"

.NOTES
General notes
#>
function Get-UEFIBootEntry {
	[CmdletBinding()]
	[OutputType([string])]
	Param (
		# Key name to search on
		[Parameter(Mandatory = $false, ParameterSetName = 'KEYVALUE')][string]$Key,
		# Value to find within key
		[Parameter(Mandatory = $false, ParameterSetName = 'KEYVALUE')][string]$Value,
		# Label of an UEFI boot entry
		[Parameter(Mandatory = $false, ParameterSetName = 'UEFILABEL')][string]$UefiLabel
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		$bcd = Get-BCD -AsGUID

		switch ($PSCmdlet.ParameterSetName) {
			'UEFILABEL' {
				$id = ($bcd.keys | Where-Object { $bcd.$_.description -eq $UefiLabel })
				return $bcd.$id
			}
			'KEYVALUE' {
				$id = ($bcd.keys | Where-Object { $bcd.$_.$Key -eq $Value })
				return $bcd.$id
			}
			default {
				return $null
			}
		}
	}

	End {
		Write-LeaveFunction
	}
}

<#

 ########    ###     ######  ##    ##     ######   ######  ##     ## ######## ########  ##     ## ##       ######## ########
    ##      ## ##   ##    ## ##   ##     ##    ## ##    ## ##     ## ##       ##     ## ##     ## ##       ##       ##     ##
    ##     ##   ##  ##       ##  ##      ##       ##       ##     ## ##       ##     ## ##     ## ##       ##       ##     ##
    ##    ##     ##  ######  #####        ######  ##       ######### ######   ##     ## ##     ## ##       ######   ########
    ##    #########       ## ##  ##            ## ##       ##     ## ##       ##     ## ##     ## ##       ##       ##   ##
    ##    ##     ## ##    ## ##   ##     ##    ## ##    ## ##     ## ##       ##     ## ##     ## ##       ##       ##    ##
    ##    ##     ##  ######  ##    ##     ######   ######  ##     ## ######## ########   #######  ######## ######## ##     ##

#>

<#
.SYNOPSIS
Create a new folder in the Operating System's task scheduler

.DESCRIPTION
Create a new folder in the Operating System's task scheduler daemon.

.PARAMETER FolderName
The name of the folder to create

.PARAMETER Root
An optional root folder to be the parent of the folder to create

.EXAMPLE
New-PwShFwScheduledTaskFolder -FolderName "MyDaemon"

.NOTES
On linux, this function just create an empty file under /etc/cron.d by default
#>
function New-PwShFwScheduledTaskFolder {
	[CmdletBinding()]
	[OutputType([Boolean])]
	[Alias('New-CronTaskFile')]
	Param (
		[Alias('Name')]
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$FolderName,
		[string]$Root = '\'
	)
	Begin {
		Write-EnterFunction
	}

	Process {
 		$ErrorActionPreference = "stop"
		$scheduleObject = New-Object -ComObject schedule.service
		$scheduleObject.connect()
		$rootFolder = $scheduleObject.GetFolder($Root)
		Try {
		   $null = $scheduleObject.GetFolder($FolderName)
		} Catch {
			$null = $rootFolder.CreateFolder($FolderName)
		} Finally {
			$ErrorActionPreference = "continue"
		}

		# test to return correct value
		Try {
			$null = $scheduleObject.GetFolder($FolderName)
			return $true
		} Catch {
			return $false
		}
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Create a new scheduled task / cron job

.DESCRIPTION
New-PwShFwScheduledTask function is a cross-platform wrapper for Scheduled Task / cron jobs.
It creates a full scheduled task / cron job within one command whatever the underlying OS is.

.PARAMETER Folder
Optional Folder name into which to create the task

.PARAMETER Name
Name of the task

.PARAMETER Command
Command or script to run

.PARAMETER Parameters
Arguments to pass to the command

.PARAMETER Description
Optional description of the task

.PARAMETER Username
Username to use to launch the task

.PARAMETER Minutes
Set the minutes number to trigger job (can be '*' to run every minutes).
Default = '*'

.PARAMETER Hour
Set the hour number to trigger job (can be '*' to run every hours).
Default = '*'

.PARAMETER DayOfMonth
Set the day of the month number to trigger job (can be '*' to run every days).
Default = '*'

.PARAMETER DayOWeek
Set the day of week number to trigger job (can be '*' to run every days of week).
Default = '*'

.PARAMETER Month
Set the month number to trigger job (can be '*' to run every months).
Default = '*'

.PARAMETER Year
Set the year number to trigger job (can be '*' to run every years).
Default = '*'

.PARAMETER EveryMinutes
Equals to -Minute '*'

.PARAMETER Hourly
Equals to -Minute 0 -Hours '*'

.PARAMETER Daily
Equals to -Minute 0 -Hours 0 -DayOfMonth '*'

.PARAMETER Weekly
Equals to -Minute 0 -Hours 0 -DayOfWeek 1 -Week '*'

.PARAMETER Monthly
Equals to -Minute 0 -Hours 0 -DayOfMonth 1 -Month '*'

.PARAMETER Yearly
Equals to -Minute 0 -Hours 0 -DayOfMonth 1 -Month 1 -Year '*'

.PARAMETER AtStartup
Trigger job on system startup

.PARAMETER AtLogon
Trigger job on session logon

.EXAMPLE
An example

.NOTES
General notes
#>
function New-PwShFwScheduledTask {
	[CmdletBinding(DefaultParameterSetName = 'EXACT_TRIGGER')]
	[OutputType([String])]
	Param (
		[string]$Folder = "\",
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Name,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Command,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $true)][string]$Parameters,
		[string]$Description = "",
		[string]$Username = $env:USERNAME,
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Minute = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Hour = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$DayOfMonth = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$DayOfWeek = '*',
		# [Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Week = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Month = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Year = '*',
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$EveryMinutes,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Hourly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Daily,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Weekly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Monthly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Yearly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$AtStartup,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$AtLogon
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		if ([string]::IsNullOrEmpty($Description)) { $Description = "$Name - $Command $Parameters" }
		$action = New-ScheduledTaskAction -Execute $Command -Argument $Parameters
		switch ($PSCmdlet.ParameterSetName) {
			'EXACT_TRIGGER' {
			}
			'ALIAS_TRIGGER' {
				if ($EveryMinutes)	{ $trigger = New-ScheduledTaskTrigger -Once -At 0am -RepetitionInterval (New-TimeSpan -Seconds 60) -RepetitionDuration ([System.TimeSpan]::MaxValue) }
				if ($Hourly)		{ $trigger = New-ScheduledTaskTrigger -Once -At 0am -RepetitionInterval (New-TimeSpan -Minutes 60) -RepetitionDuration ([System.TimeSpan]::MaxValue) }
				if ($Daily)			{ $trigger = New-ScheduledTaskTrigger -Daily -At 0am }
				if ($Weekly)		{ $trigger = New-ScheduledTaskTrigger -Weekly -DaysOfWeek $DayOfWeek -At 0am }
				if ($Monthly)		{ $trigger = New-ScheduledTaskTrigger -Daily -DaysInterval 30 -At 0am }
				if ($Yearly)		{ $trigger = New-ScheduledTaskTrigger -Daily -DaysInterval 365 -At 0am }
				if ($AtStartup)		{ $trigger = New-ScheduledTaskTrigger -AtStartup }
				if ($AtLogon)		{ $trigger = New-ScheduledTaskTrigger -AtLogon }
			}
		}
		try {
			$task = Register-ScheduledTask -TaskPath $Folder -TaskName $Name -User $Username -Description $Description -Action $action -Trigger $trigger
			$rc = $?
		} catch {
			eerror $_
			$rc = $false
		}
		return $rc
	}

	End {
		Write-LeaveFunction
	}
}

<#

 ########  ########  ######   ####  ######  ######## ########  ##    ##
 ##     ## ##       ##    ##   ##  ##    ##    ##    ##     ##  ##  ##
 ##     ## ##       ##         ##  ##          ##    ##     ##   ####
 ########  ######   ##   ####  ##   ######     ##    ########     ##
 ##   ##   ##       ##    ##   ##        ##    ##    ##   ##      ##
 ##    ##  ##       ##    ##   ##  ##    ##    ##    ##    ##     ##
 ##     ## ########  ######   ####  ######     ##    ##     ##    ##

#>

# @var		RegistryLoaded
# @brief	Hashtable to remember what is loaded where
# @description	Each time an offline registry file is loaded into current registry, record this in the hashtable.
#				The format is { "/full/path/to/filename" : "HKLM:\path\to\hive" }
$RegistryLoaded = @{}

<#
.SYNOPSIS
Load list of current offline registry loaded into live registry

.DESCRIPTION
The list is a hashtable saved at "HKLM:\Software\PwSh.Fw\pwsh.fw.os" folder into "RegistryLoaded" property

.NOTES
It is an internal function. Do not use.
#>

function Load-OfflineWindowsRegistryMountPoint {
	[CmdletBinding()]
	[OutputType([hashtable])]
	Param (
		# [Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$string
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		[hashtable]$RegistryLoaded = @{}
		if (Test-RegKeyExist "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded") {
			$keys = Get-Item "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded" | Select-Object -ExpandProperty property
			foreach ($k in $keys) {
				$kUnescaped = [regex]::unescape($k)
				$RegistryLoaded.$kUnescaped = Get-ItemPropertyValue -Path "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded" -Name $k
			}
		}
		return $RegistryLoaded
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Save list of current offline registry loaded into live registry

.DESCRIPTION
The list is a hashtable saved at "HKLM:\Software\PwSh.Fw\pwsh.fw.os" folder into "RegistryLoaded" property

.NOTES
It is an internal function. Do not use.
#>
function Save-NewOfflineWindowsRegistryMountPoint {
	[CmdletBinding()]
	[OutputType([void])]
	Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Filename,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$MountPoint
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		try {
			[hashtable]$RegistryLoaded = Load-OfflineWindowsRegistryMountPoint
		} catch {
			[hashtable]$RegistryLoaded = @{}
		}
		$Filename = [regex]::Escape($Filename)
		# $RegistryLoaded.$Filename = [regex]::Escape($MountPoint)
		$null = New-Item -Path "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded" -ItemType directory -ErrorAction SilentlyContinue
		$null = New-ItemProperty -Path "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded" -Name $Filename -Value $MountPoint -Force
	}

	End {
		Write-LeaveFunction
	}
}

function Save-RemoveOfflineWindowsRegistryMountPoint {
	[CmdletBinding()]
	[OutputType([void])]
	Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true, ParameterSetName = 'FILENAME')][string]$Filename,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true, ParameterSetName = 'MountPoint')][string]$MountPoint
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		try {
			[hashtable]$RegistryLoaded = Load-OfflineWindowsRegistryMountPoint
		} catch {
			[hashtable]$RegistryLoaded = @{}
		}
		switch ($PSCmdlet.ParameterSetName) {
			'FILENAME' {
				$key = [regex]::Escape($Filename)
			}
			'MountPoint' {
				$key = $RegistryLoaded.keys | Where-Object { $RegistryLoaded.$_ -eq $MountPoint }
				$key = [regex]::Escape($key)
			}
		}
		Remove-ItemProperty -Path "HKLM:\Software\PwSh.Fw\pwsh.fw.os\RegistryLoaded" -Name $key -Force
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Mount a windows registry file

.DESCRIPTION
Mount a windows registry file like user.dat, SYSTEM or SOFTWARE from offline windows disk, or even BCD file.
By default, content of this registry is mounted under HKLM:\{Generated GUID} registry key.

.PARAMETER File
Full path to a windows registry file. Must be used alone.

.PARAMETER Path
Full path to an offline windows installation folder. Must be used with -Hive parameter.

.PARAMETER Hive
Name of a windows hive. Currently, only SOFTWARE and SYSTEM are supported. Must be used with -Path parameter

.PARAMETER MountPoint
Override default HKLM:\$GUID MountPoint by explicitly specifying MountPoint. Must be full registry path e.g. "HKLM:\Some_Path"

.EXAMPLE
Mount-OfflineWindowsRegistry -Path E:\Windows -Hive SOFTWARE
This example will mount HKLM\SOFTWARE registry hive from an offline windows disk connected as E:

.EXAMPLE
Mount-OfflineWindowsRegistry -File E:\Users\<username>\user.dat
This example will mount a user profile from the same offline windows disk

.OUTPUTS
Registry path to where the registry file is mounted

.NOTES
General notes
#>

function Mount-OfflineWindowsRegistry {
    [CmdletBinding(
		DefaultParameterSetName="HIVE"
	)]Param(
		# Parameter help description
		[Parameter(ParameterSetName="FILENAME",ValueFromPipeLine = $true)]
        [ValidateScript({
            if(-Not ($_ | Test-Path) ){
                throw "File not found."
            }
            if(-Not ($_ | Test-Path -PathType Leaf) ){
                throw "The File argument must be a filename."
            }
            return $true
		})][System.IO.FileInfo]$File = "X:\Windows\System32\config\SOFTWARE",

		[Parameter(ParameterSetName="HIVE",ValueFromPipeLine = $true)]
        [ValidateScript({
            if(-Not ($_ | Test-Path) ){
                throw "File or folder does not exist"
            }
            if(-Not ($_ | Test-Path -PathType Container) ){
                throw "The Path argument must be a Folder."
            }
            return $true
		})][System.IO.DirectoryInfo]$Path = "X:\Windows",

		[Parameter(ParameterSetName="HIVE")]
		[ValidateSet('SOFTWARE','SYSTEM')]
		[string]$Hive = "SOFTWARE",

		[string]$MountPoint
    )
    Begin {
        Write-EnterFunction
		$RegistryLoaded = Load-OfflineWindowsRegistryMountPoint
    }

    Process {
		switch ($PSCmdlet.ParameterSetName) {
			"FILENAME" {
				$Hive = $File.BaseName
				$filename = $File.FullName
			}
			"HIVE" {
				$filename = $Path.FullName + "\System32\config\" + $Hive
			}
		}

		# return registered MountPoint if file is already loaded
		if ($null -ne $RegistryLoaded.$filename) {
			$MountPoint = $RegistryLoaded.$filename
			if (Test-RegKeyExist -RegPath $MountPoint) {
				return $MountPoint
			} else {
				Write-Warning "Registry already loaded but MountPoint not found. Try to reload."
			}
		}
		if ([string]::IsNullOrEmpty($MountPoint)) {
			$MountPoint = "HKLM:\$(New-Guid)"
		}
		if (fileExist -Name $filename) {
			try {
				$rc = Execute-Command reg load $MountPoint.Replace(':', '') $filename
			} catch {
				Write-Error $_
			}
			if (Test-RegKeyExist -RegPath $MountPoint) {
				$null = Save-NewOfflineWindowsRegistryMountPoint -File $filename -MountPoint $MountPoint
			}
		} else {
			Write-Error "Cannot find registry hive at $($Path.FullName)"
		}

		return $MountPoint
    }

    End {
        Write-LeaveFunction
    }
}

<#
    .SYNOPSIS
	Unmount a windows registry file

    .DESCRIPTION
    Umount a windows registry file previously mounted with Mount-OfflineWindowsRegistry.

    .EXAMPLE
    New-TemplateFunction -string "a string"

#>
function DisMount-OfflineWindowsRegistry {
    [CmdletBinding(
		DefaultParameterSetName = "HIVE"
	)]Param (
		# MountPoint is a registry path where the file is mounted
		[Alias('RegPath')]
		[Parameter(ParameterSetName="HIVE",ValueFromPipeLine = $true)]
        [string]$MountPoint,

		# Full path and filename to the mounted file
		[Parameter(ParameterSetName="FILE",ValueFromPipeLine = $true)]
        [string]$File,

		# Dismount all files mounted
		[Parameter(ParameterSetName="All",ValueFromPipeLine = $true)]
        [switch]$All
    )
    Begin {
        Write-EnterFunction
		$RegistryLoaded = Load-OfflineWindowsRegistryMountPoint
    }

    Process {
		switch ($PSCmdlet.ParameterSetName) {
			"FILENAME" {
				[array]$MountPoints = @($RegistryLoaded.$File)
			}
			"HIVE" {
				[array]$MountPoints = @($MountPoint)
			}
			"All" {
				[array]$MountPoints = $RegistryLoaded.Values
			}
		}
		if ($MountPoints.count -eq 0) {
			Write-Error "No offline registry found."
			return [void]
		}
		$MountPoints | ForEach-Object {
			# Write-Devel "_ = $_"
			$mp = $_
			# Write-Devel "mp = $mp"
			$DosRegPath = $mp.Replace(':', '')
			$rc = Execute-Command reg unload $DosRegPath
			if (Test-RegKeyExist -RegPath $mp) {
				Write-Error "Unable to unload registry key at '$mp'."
			} else {
				Save-RemoveOfflineWindowsRegistryMountPoint -MountPoint $mp
			}
		}
    }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS
Get files loaded in the registry

.DESCRIPTION
Return the list of files currently loaded in the registry.
Only files loaded with Mount-OfflineWindowsRegistry can be listed

.EXAMPLE
Get-OfflineWindowsRegistry

.NOTES
General notes
#>
function Get-OfflineWindowsRegistry {
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(ParameterSetName="HIVE",ValueFromPipeLine = $true)]
        [string]$RegPath,
		[Parameter(ParameterSetName="FILE",ValueFromPipeLine = $true)]
        [string]$File,
		[Parameter(ParameterSetName="All",ValueFromPipeLine = $true)]
        [switch]$All
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		switch ($PSCmdlet.ParameterSetName) {
			"FILENAME" {
				return $RegistryLoaded.$File
			}
			"HIVE" {
				return $RegistryLoaded.Keys | Where-Object { $RegistryLoaded[$_] -eq $RegPath }
			}
			"All" {
				return $RegistryLoaded
			}
		}
	}

	End {
		Write-LeaveFunction
	}
}
