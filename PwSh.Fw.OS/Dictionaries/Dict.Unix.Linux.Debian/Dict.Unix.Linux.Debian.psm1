
# List here the name of the package to add to system
# . fuseiso : mount ISO files as non-root user
# . libhivex-bin : read Windows registry files
# . mount : for 'losetup' (should already be installed)
# . util-linux : for 'lsblk' (should already be installed)
# . wimtools : mnipulate wim files
$Script:PackageList = "fuseiso libhivex-bin mount util-linux wimtools"

<#

 ########     ###     ######  ##    ##    ###     ######   ########
 ##     ##   ## ##   ##    ## ##   ##    ## ##   ##    ##  ##
 ##     ##  ##   ##  ##       ##  ##    ##   ##  ##        ##
 ########  ##     ## ##       #####    ##     ## ##   #### ######
 ##        ######### ##       ##  ##   ######### ##    ##  ##
 ##        ##     ## ##    ## ##   ##  ##     ## ##    ##  ##
 ##        ##     ##  ######  ##    ## ##     ##  ######   ########

#>

<#
.SYNOPSIS
Install a package on debian-like distribution

.DESCRIPTION
Install a package using apt command

.EXAMPLE
Install-OSPackage -Name firefox

.NOTES
General notes
#>
function Install-OSPackage {
	[CmdletBinding(SupportsShouldProcess = $true, ConfirmImpact = 'Medium')]
	[OutputType([boolean])]
	Param (
		# The name of the package to install
		[Alias('Name')]
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$PackageName
	)
	Begin {
		Write-EnterFunction
		$command = "DEBIAN_FRONTEND=noninteractive apt -y install --no-install-recommends"
	}

	Process {
		$command += " $PackageName"
	}

	End {
		if ($PSCmdlet.ShouldProcess("Install package ?")) {
			$rc = Execute-Command -exe "bash" -args "-c `"$command`""
		} else {
			$rc = $false
		}
		return $rc
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Uninstall a package from disk

.DESCRIPTION
Uninstall a package using apt command

.EXAMPLE
Uninstall-OSPackage -Name firefox

.EXAMPLE
Uninstall-OSPackage -Name firefox -Purge

.NOTES
General notes
#>
function Uninstall-OSPackage {
	[CmdletBinding(SupportsShouldProcess = $true)]
	[OutputType([boolean])]
	Param (
		# The name of the package to install
		[Alias('Name')]
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$PackageName,

		# Purge configuration
		[switch]$Purge
	)
	Begin {
		Write-EnterFunction
		$command = "DEBIAN_FRONTEND=noninteractive apt -y remove"
		if ($Purge) { $command += " --purge" }
	}

	Process {
		$command += " $PackageName"
	}

	End {
		if ($PSCmdlet.ShouldProcess("Uninstall package ?")) {
			$rc = Execute-Command -exe "bash" -args "-c `"$command`""
		} else {
			$rc = $false
		}
		return $rc
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Install dependencies to unlock full PWSH functionnalities

.DESCRIPTION
PWSH can make use of some binaries that are not included in distro at install time.
This function can install them all at once.

#>
function Install-Dependencies {
	# [CmdletBinding()]Param (
	# 	[Parameter(Mandatory = $true,ValueFromPipeLine = $true)][string]$string = ""
    # )
    Begin {
        Write-EnterFunction
    }

    Process {
		ebegin("Installing dependancies. Please wait")
		eexec apt-get install -y $Script:PackagesList
		eend $?
    }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS
Remove dependencies from current system

.DESCRIPTION
Cleanly remove dependencies
# TODO add this function to the uninstall process

#>
function Remove-Dependencies {
	# [CmdletBinding()]Param (
	# 	[Parameter(Mandatory = $true,ValueFromPipeLine = $true)][string]$string = ""
    # )
    Begin {
        Write-EnterFunction
    }

    Process {
		ebegin("Removing dependancies. Please wait")
		eexec apt-get purge -y $Script:PackagesList
		eexec apt-get autoremove -y
		eexec apt-get autoclean -y
		eend $?
    }

    End {
        Write-LeaveFunction
    }
}

<#

 ####  ######   #######
  ##  ##    ## ##     ##
  ##  ##       ##     ##
  ##   ######  ##     ##
  ##        ## ##     ##
  ##  ##    ## ##     ##
 ####  ######   #######

#>

<#
.SYNOPSIS
Mount an .iso file somewhere on the filesystem

.DESCRIPTION
Mount-Iso take care if Iso file is already mounted.
If not, it tries to mount it in /mnt directory.
It return absolute path name of mountpoint.

.EXAMPLE
Mount-Iso -File /path/to/cdrom.iso
Mount the iso '/path/to/cdrom.iso' into the filesystem

.EXAMPLE
Mount-Iso -File /path/to/cdrom.iso -To /tmp/myiso
Mount the iso '/path/to/cdrom.iso' into the /tmp/myiso directory

.OUTPUTS
Absolute pathname of the mountpoint of the iso file

.NOTES
# TODO make Mount-Iso work as non-root user. It should be OK with fuseiso, but it can't mount UDF ISO files so it is not reliable
#>
function Mount-Iso {
    [CmdletBinding()][OutputType([System.Boolean])]Param (
        [Parameter(Mandatory = $true,ValueFromPipeLine = $true)][string]$File,
        [string]$To = $null
    )
    Begin {
		Write-EnterFunction
		# edevel ("To = $To")
        if ($To -eq "") {
			if (IamAdmin) {
				$To = "/tmp/iso-" + $(New-Guid)
			} else {
				$To = $env:HOME + "/iso-" + $(New-Guid)
			}
		}
		# edevel ("To = $To")
    }

    Process {

		if (!(fileExist -Name $File)) { return $false }

        # $oFile = Get-Item $File
        # $To = $To + '/' + $(New-Guid)

        # check if iso file is already mounted on system
        $loop = losetup --list --json -j $File | ConvertFrom-Json
        if ($null -ne $loop) {
            $loopDevice = $loop.loopdevices[0].name
            $blkDevice = lsblk --list --json -O $loopDevice | ConvertFrom-Json
            return $blkDevice.blockdevices[0].mountpoint
        } # else continue on

        new-item -path $To -ItemType Directory -Force

		if (IamAdmin) {
			try {
				eexec -exe mount -args "-o loop $File $To"
			} catch {
				remove-item $To -Confirm:$false
				return $false
			}
		} else {
			ewarn($MyInvocation.MyCommand.ToString() + "() : You must be root.")
			return $false
			# eexec -exe fuseiso -args "'$File' '$To'"
		}

		return $To
    }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS
Unmount an .iso file

.DESCRIPTION
Dismount-Iso unmount iso file either by specifying iso filename or mountpoint.

.EXAMPLE
Dismount-Iso -File /path/to/cdrom.iso
Dismount the iso '/path/to/cdrom.iso' from the filesystem

.EXAMPLE
Dismount-Iso -From /mnt/myiso
Dismount the iso from the /mnt/myiso directory

.OUTPUTS
$true on success, $false otherwise

#>
function Dismount-Iso {
    [CmdletBinding(DefaultParameterSetName = 'MOUNTPOINT')]Param (
		[Parameter(ParameterSetName = 'FILE', Mandatory = $true,ValueFromPipeLine = $true)]
		[string]$File,
		[Parameter(ParameterSetName = 'MOUNTPOINT', Mandatory = $true,ValueFromPipeLine = $true)]
		[Alias('From')][string]$Mountpoint,
		[switch]$Force
    )
    Begin {
		Write-EnterFunction
		if ($Force) {
			$options = "-f"
		}
    }

    Process {

		switch ($PSCmdlet.ParameterSetName) {
			'FILE' {
				if (!(fileExist -Name $File)) { return $false }

				# $oFile = Get-Item $File
				# $To = $To + '/' + $(New-Guid)

				# 1st check if iso file is already mounted on system
				$loop = losetup --list --json -j $File | ConvertFrom-Json
				if ($null -ne $loop) {
					$loopDevice = $loop.loopdevices[0].name
					$blkDevice = lsblk --list --json -O $loopDevice | ConvertFrom-Json
					eexec umount $options $blkDevice.blockdevices[0].mountpoint
				} # else continue on
			}
			'MOUNTPOINT' {
				if (IamAdmin) {
					eexec umount $options $Mountpoint
				} else {
					eexec fusermount -u $Mountpoint
				}
			}
		}

		Remove-Item $Mountpoint -Recurse:$false -Confirm:$false -Force
		$rc = $?

		return $rc
    }

    End {
        Write-LeaveFunction
    }
}

<#

 ##      ## #### ##     ##
 ##  ##  ##  ##  ###   ###
 ##  ##  ##  ##  #### ####
 ##  ##  ##  ##  ## ### ##
 ##  ##  ##  ##  ##     ##
 ##  ##  ##  ##  ##     ##
  ###  ###  #### ##     ##

#>

<#
.SYNOPSIS
Mount a .wim file somewhere on the filesystem

.DESCRIPTION
Mount-wim CANNOT take care if wim file is already mounted (fuse limitation)
If not, it tries to mount it in /mnt directory.
It return absolute path name of mountpoint.

.EXAMPLE
Mount-wim -File /path/to/install.wim
Mount the wim '/path/to/install.wim' into the filesystem

.EXAMPLE
Mount-wim -File /path/to/install.wim -Index 3 -To /tmp/mywim
Mount the wim '/path/to/install.wim' into the /tmp/mywim directory

.OUTPUTS
Absolute pathname of the mountpoint of the wim file

.NOTES
Mount-wim CANNOT take care if wim file is already mounted (fuse limitation)
#>
function Mount-Wim {
    [CmdletBinding()]Param (
		[Parameter(Mandatory = $true,ValueFromPipeLine = $true)][string]$File,
		[uint16]$Index = 1,
        [string]$To = $null
    )
    Begin {
		Write-EnterFunction
		# edevel ("To = $To")
        if ($To -eq "") {
			if (IamAdmin) {
				$To = "/tmp/wim-" + $(New-Guid)
			} else {
				$To = $env:HOME + "/wim-" + $(New-Guid)
			}
		}
		# edevel ("To = $To")
    }

    Process {

		if (!(fileExist -Name $File)) { return $false }

        new-item -path $To -ItemType Directory -Force

		eexec -exe wimmount -args "$File $Index $To"

		return $To
    }

    End {
        Write-LeaveFunction
    }
}

<#
.SYNOPSIS
Unmount an .wim file

.DESCRIPTION
Dismount-wim unmount wim file either by specifying wim filename or mountpoint.

.EXAMPLE
Dismount-wim -File /path/to/cdrom.wim
Dismount the wim '/path/to/cdrom.wim' from the filesystem

.EXAMPLE
Dismount-wim -From /mnt/mywim
Dismount the wim from the /mnt/mywim directory

.OUTPUTS
$true on success, $false otherwise

#>
function Dismount-Wim {
    [CmdletBinding(DefaultParameterSetName = 'MOUNTPOINT')][OutputType([System.Boolean])]Param (
		# [Parameter(ParameterSetName = 'FILE', Mandatory = $true,ValueFromPipeLine = $true)]
		# [string]$File,
		[Parameter(ParameterSetName = 'MOUNTPOINT', Mandatory = $true,ValueFromPipeLine = $true)]
		[Alias('From')][string]$Mountpoint
		# [switch]$Force
    )
    Begin {
		Write-EnterFunction
		if ($Force) {
			$options = "-f"
		}
    }

    Process {

		switch ($PSCmdlet.ParameterSetName) {
			'FILE' {
				if (!(fileExist -Name $File)) { return $false }

				# $oFile = Get-Item $File
				# $To = $To + '/' + $(New-Guid)

				# 1st check if wim file is already mounted on system
				$loop = losetup --list --json -j $File | ConvertFrom-Json
				if ($null -ne $loop) {
					$loopDevice = $loop.loopdevices[0].name
					$blkDevice = lsblk --list --json -O $loopDevice | ConvertFrom-Json
					eexec umount $options $blkDevice.blockdevices[0].mountpoint
				} # else continue on
			}
			'MOUNTPOINT' {
				eexec fusermount -u $Mountpoint
			}
		}

		Remove-Item $Mountpoint -Recurse:$false -Confirm:$false -Force
		$rc = $?

		return $rc
    }

    End {
        Write-LeaveFunction
    }
}

<#

  ######   #######  ##     ## ########  ##     ## ######## ######## ########
 ##    ## ##     ## ###   ### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## #### #### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## ## ### ## ########  ##     ##    ##    ######   ########
 ##       ##     ## ##     ## ##        ##     ##    ##    ##       ##   ##
 ##    ## ##     ## ##     ## ##        ##     ##    ##    ##       ##    ##
  ######   #######  ##     ## ##         #######     ##    ######## ##     ##

#>
