<#

  ######   #######  ##     ## ########  ##     ## ######## ######## ########
 ##    ## ##     ## ###   ### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## #### #### ##     ## ##     ##    ##    ##       ##     ##
 ##       ##     ## ## ### ## ########  ##     ##    ##    ######   ########
 ##       ##     ## ##     ## ##        ##     ##    ##    ##       ##   ##
 ##    ## ##     ## ##     ## ##        ##     ##    ##    ##       ##    ##
  ######   #######  ##     ## ##         #######     ##    ######## ##     ##

#>

<#
.SYNOPSIS
Know if computer was booted using old legacy BIOS mode or new UEFI.

.DESCRIPTION
To make next boot efficient, we need to know in what mode the computer booted to this stage.
This function will tell us that.
If every tests fail, or if we can't determined the boot mode, it default to "BIOS" mode.

.EXAMPLE
$bootMode = Get-ComputerBootMode

.NOTES
General notes
#>
function Get-ComputerBootMode {
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		# [Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$string
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		# Mac are UEFI only since a while now.
		return "UEFI"
	}

	End {
		Write-LeaveFunction
	}
}

<#

 ########    ###     ######  ##    ##     ######   ######  ##     ## ######## ########  ##     ## ##       ######## ########
    ##      ## ##   ##    ## ##   ##     ##    ## ##    ## ##     ## ##       ##     ## ##     ## ##       ##       ##     ##
    ##     ##   ##  ##       ##  ##      ##       ##       ##     ## ##       ##     ## ##     ## ##       ##       ##     ##
    ##    ##     ##  ######  #####        ######  ##       ######### ######   ##     ## ##     ## ##       ######   ########
    ##    #########       ## ##  ##            ## ##       ##     ## ##       ##     ## ##     ## ##       ##       ##   ##
    ##    ##     ## ##    ## ##   ##     ##    ## ##    ## ##     ## ##       ##     ## ##     ## ##       ##       ##    ##
    ##    ##     ##  ######  ##    ##     ######   ######  ##     ## ######## ########   #######  ######## ######## ##     ##

#>

<#
.SYNOPSIS
Create a new folder in the Operating System's task scheduler

.DESCRIPTION
Create a new folder in the Operating System's task scheduler daemon.

.PARAMETER FolderName
The name of the folder to create

.PARAMETER Root
An optional root folder to be the parent of the folder to create

.EXAMPLE
New-PwShFwScheduledTaskFolder -FolderName "MyDaemon"

.NOTES
On linux, this function just create an empty file under /usr/lib/cron/jobs by default
#>
function New-PwShFwScheduledTaskFolder {
	[CmdletBinding()]
	[OutputType([Boolean])]
	[Alias('New-CronTaskFile')]
	Param (
		[Alias('Name')]
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$FolderName,
		[string]$Root = '/usr/lib/cron/tabs'
	)
	Begin {
		Write-EnterFunction
	}

	Process {
 		# $ErrorActionPreference = "stop"
		# Try {
		#    $null = New-Item -Path $Root -Name $FolderName -ItemType File
		# } Catch {
		# } Finally {
		# 	$ErrorActionPreference = "continue"
		# }

		# # test to return correct value
		# if (Test-FileExist "$Root/$FolderName") {
		# 	return $true
		# } else {
		# 	return $false
		# }
		# macOS does not support folder
		return $true
	}

	End {
		Write-LeaveFunction
	}
}

<#
.SYNOPSIS
Create a new scheduled task / cron job

.DESCRIPTION
New-PwShFwScheduledTask function is a cross-platform wrapper for Scheduled Task / cron jobs.
It creates a full scheduled task / cron job within one command whatever the underlying OS is.

.PARAMETER Folder
Optional Folder name into which to create the task

.PARAMETER Name
Name of the task

.PARAMETER Command
Command or script to run

.PARAMETER Parameters
Arguments to pass to the command

.PARAMETER Description
Optional description of the task

.PARAMETER Username
Username to use to launch the task
default = root

.PARAMETER Minutes
Set the minutes number to trigger job (can be '*' to run every minutes).
Default = '*'

.PARAMETER Hour
Set the hour number to trigger job (can be '*' to run every hours).
Default = '*'

.PARAMETER DayOfMonth
Set the day of the month number to trigger job (can be '*' to run every days).
Default = '*'

.PARAMETER DayOWeek
Set the day of week number to trigger job (can be '*' to run every days of week).
Default = '*'

.PARAMETER Month
Set the month number to trigger job (can be '*' to run every months).
Default = '*'

.PARAMETER Year
Set the year number to trigger job (can be '*' to run every years).
Default = '*'

.PARAMETER EveryMinutes
Equals to -Minute '*'

.PARAMETER Hourly
Equals to -Minute 0 -Hours '*'

.PARAMETER Daily
Equals to -Minute 0 -Hours 0 -DayOfMonth '*'

.PARAMETER Weekly
Equals to -Minute 0 -Hours 0 -DayOfWeek 1 -Week '*'

.PARAMETER Monthly
Equals to -Minute 0 -Hours 0 -DayOfMonth 1 -Month '*'

.PARAMETER Yearly
Equals to -Minute 0 -Hours 0 -DayOfMonth 1 -Month 1 -Year '*'

.PARAMETER AtStartup
Trigger job on system startup

.PARAMETER AtLogon
Trigger job on session logon

.EXAMPLE
An example

.NOTES
General notes
#>
function New-PwShFwScheduledTask {
	[CmdletBinding(DefaultParameterSetName = 'EXACT_TRIGGER')]
	[OutputType([String])]
	Param (
		[string]$Folder,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Name,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Command,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $true)][string]$Parameters,
		[string]$Description,
		[string]$Username = $env:USER,
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Minute = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Hour = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$DayOfMonth = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$DayOfWeek = '*',
		# [Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Week = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Month = '*',
		[Parameter(ParameterSetName = 'EXACT_TRIGGER')][string]$Year = '*',
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$EveryMinutes,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Hourly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Daily,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Weekly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Monthly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$Yearly,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$AtStartup,
		[Parameter(ParameterSetName = 'ALIAS_TRIGGER')][switch]$AtLogon
	)
	Begin {
		Write-EnterFunction
		$Filename = $Folder ? "/usr/lib/cron/tabs/$Username" : "/usr/lib/cron/$Username"
	}

	Process {
		if ([string]::IsNullOrEmpty($Description)) { $Description = "$Name - $Command $Parameters" }
		$trigger = ""
		switch ($PSCmdlet.ParameterSetName) {
			'EXACT_TRIGGER' {
				$trigger = "$Minute $Hour $DayOfMonth $Month $DayOfWeek"
			}
			'ALIAS_TRIGGER' {
				if ($EveryMinutes)	{ $trigger = "* * * * *" }
				if ($Hourly)		{ $trigger = "0 * * * *" }
				if ($Daily)			{ $trigger = "0 0 * * *" }
				if ($Weekly)		{ $trigger = "0 0 * * 0" }
				if ($Monthly)		{ $trigger = "0 0 1 * *" }
				if ($Yearly)		{ $trigger = "0 0 1 1 *" }
				if ($AtStartup)		{ $trigger = "@reboot" }
			}
		}

		$task = @"
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
# $Description
$trigger $Username $Command $Parameters
"@
		try {
			$task | Out-File -FilePath $Filename -Encoding utf8
			$rc = $?
		} catch {
			eerror $_
			$rc = $false
		}

		return $rc
	}

	End {
		Write-LeaveFunction
	}
}
