<#
.SYNOPSIS
Test if a path is an Operating System's root

.DESCRIPTION
The root of an Operating System can be determined if it contains some well known structure.
This function test for the minimum of a known structure to see if the given Path is an Operating System root.

.PARAMETER Path
Path to check

.EXAMPLE
Test-IsOSRoot -Path 'c:\'

.EXAMPLE
"c:" | Test-IsOSRoot

.EXAMPLE
"/mnt/remote/root" | Test-IsOSRoot

.NOTES
General notes
#>

function Test-IsOSRoot {
	[CmdletBinding()]
	[OutputType([Boolean])]
	Param(
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Path
    )

	begin {

	}

	process {
		if (Test-Path "$Path/boot" -PathType Container) {
			return $true
		}
		if (Test-Path "$Path/Windows/System32" -PathType Container) {
			return $true
		}
		return $false
	}

	end {

	}
}

<#
.SYNOPSIS
Get the root path of an OS.

.DESCRIPTION
Find the root folder containing an Operating System.

.PARAMETER Path
Find the OS under this Path and return its root path

.PARAMETER Online
Find the OS currently online.

.EXAMPLE
Get-OSRoot -Online

This example will likely return 'C:' on windows, '/' on any Unix-like operating system, includin linux, macOS...

.EXAMPLE
Get-OSRoot -Path "/mnt/sda2/var/lib"

This example will return '/mnt/sda2' if this disk hosts a Unix-like Operating System.

.NOTES
General notes
#>

function Get-OSRoot {
    [CmdletBinding(DefaultParameterSetName="ONLINE")][OutputType([String])]Param (
		[Parameter(ParameterSetName = "PATH", Mandatory = $true, ValueFromPipeLine = $true)][string]$Path,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online
    )

	begin {

	}

	process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				try {
					$Root = ((Get-PSProvider Filesystem)[0].Drives | Sort-Object -Property root)[0].Root
				} catch {
					if ($env:SystemDrive) {
						$Root = $env:SystemDrive
					} else {
						$Root = '/'
					}
				}
			}
			'PATH' {
				try {
					do {
						$Root = $Path
						$rc = Test-IsOSRoot -Path $Root
						$Path = (get-item $Path -ErrorAction stop).Parent.Fullname
					} until (($rc -eq $true) -or ([string]::IsNullOrEmpty($Path)))
					if (!$rc) {
						$Root = ""
					}
				} catch {
					$Root = ""
				}
			}
		}
		$Root = $Root.Trim('/\')
		if ($Root -eq '') { $Root = '/' }
		return $Root
	}

	end {

	}
}

<#
.SYNOPSIS
Get the platform identifier of an OS.

.DESCRIPTION
The platform identifier is the meta-family of an OS. At this time it can be 'unix', or 'windows'.

.PARAMETER Root
The root path under wich to look for the OS

.PARAMETER Online
Specify to look at the currently running OS

.EXAMPLE
Get-OSPlatform -Online

.EXAMPLE
Get-OSPlatform -Root '/mnt/sda2'

.NOTES
General notes
#>

function Get-OSPlatform {
    [CmdletBinding(DefaultParameterSetName="ROOT")][OutputType([String])]Param (
		[Parameter(ParameterSetName = "ROOT", Mandatory = $true, ValueFromPipeLine = $true)][string]$Root,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online
    )

	begin {

	}

	process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				$Root = Get-OSRoot -Online
			}
			'ROOT' {
				$Root = Get-OSRoot -Path $Root
			}
		}

		$obj = "unknown"
		if (Test-Path -Path "$Root/etc" -Type Container) {
			$obj = "unix"
		} elseif (Test-Path -Path "$Root/Windows/System32" -Type Container) {
			$obj = "windows"
		} else {
			Write-MyWarning -Message "Unknown platform. Is there an OS on the path '$Root' ?"
		}
		return $obj
	}

	end {

	}
}

<#
.SYNOPSIS
Get the kernel name of an OS

.DESCRIPTION
Identify the kernel name os an OS. The kernel name can help identifying an OS more deeply.
Wether it is 'Win32', 'WinNT', 'Darwin' and of course 'Linux'

.PARAMETER Root
The root path under wich to look for the OS

.PARAMETER Online
Specify to look at the currently running OS

.EXAMPLE
Get-OSKernel -Online

.EXAMPLE
Get-OSKernel -Root '/mnt/sda2'

.NOTES
General notes
#>

function Get-OSKernel {
    [CmdletBinding(DefaultParameterSetName="ROOT")][OutputType([String])]Param (
		[Parameter(ParameterSetName = "ROOT", Mandatory = $true, ValueFromPipeLine = $true)][string]$Root,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online
    )

	begin {

	}

	process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				$Root = Get-OSRoot -Online
			}
			'ROOT' {
				$Root = Get-OSRoot -Path $Root
			}
		}

		# edevel "Root = $Root"
		$Root = $Root.Trim('/')
		$obj = "unknown"

		# Linux
		if (Test-Path -Path "$Root/boot/bzImage*" -Type Leaf)	{	$obj = "Linux" }
		if (Test-Path -Path "$Root/boot/initrd-*" -Type Leaf)	{	$obj = "Linux" }
		if (Test-Path -Path "$Root/boot/initramfs-*" -Type Leaf)	{	$obj = "Linux" }
		if (Test-Path -Path "$Root/boot/vmlinuz*" -Type Leaf)	{	$obj = "Linux" }
		if (Test-Path -Path "$Root/boot/kernel*.img" -Type Leaf)	{	$obj = "Linux" }

		# macOS
		if (Test-Path -Path "$Root/System/Library/Kernels/kernel" -Type Leaf)	{	$obj = "Darwin" }

		# Windows
		# found informations @url https://www.forensicswiki.org/wiki/Determining_OS_version_from_an_evidence_image
		# WINDOWS 95/98/ME
		if (Test-Path -Path "$Root/MSDOS.SYS" -Type Leaf)	{	$obj = "Win32" }
		# WINDOWS NT
		# if (Test-Path -Path "/Windows/System32/kernel32.dll" -Type Leaf)	{	$obj = "win32" }
		if (Test-Path -Path "$Root/Windows/System32/ntoskrnl.exe" -Type Leaf)	{	$obj = "WinNT" }

		return $obj
	}

	end {

	}
}

<#
.SYNOPSIS
Get the mainstream branch of an OS

.DESCRIPTION
The mainstream branch is one of 'Windows', 'macOS', 'Linux' kind of OS.

.PARAMETER Root
The root path under wich to look for the OS

.PARAMETER Online
Specify to look at the currently running OS

.EXAMPLE
Get-OSMainstream -Online

.EXAMPLE
Get-OSMainstream -Root '/mnt/sda2'

.EXAMPLE
Get-OSMainstream -Kernel "Darwin"

.NOTES
General notes
#>

function Get-OSMainstream {
    [CmdletBinding(DefaultParameterSetName="ROOT")][OutputType([String])]Param (
		[Parameter(ParameterSetName = "ROOT", Mandatory = $true, ValueFromPipeLine = $true)][string]$Root,
		[Parameter(ParameterSetName = "ONLINE", Mandatory = $true, ValueFromPipeLine = $true)][switch]$Online,
		[Parameter(ParameterSetName = "KERNEL", Mandatory = $true, ValueFromPipeLine = $true)][string]$Kernel
    )

	begin {

	}

	process {
		switch ($PSCmdlet.ParameterSetName) {
			'ONLINE' {
				$Kernel = Get-OSKernel -Online
			}
			'ROOT' {
				$Kernel = Get-OSKernel -Root $Root
			}
		}

		$obj = "unknown"

		switch -Wildcard ($Kernel) {
			'Darwin' {
				$obj = "macOS"
			}
			'Linux' {
				$obj = "Linux"
			}
			'Win*' {
				$obj = "Windows"
			}
		}
		return $obj
	}

	end {

	}
}

<#
.SYNOPSIS
Check if a given path is the currently running OS

.DESCRIPTION
This function test if the OS relying on Root path is the currently online OS.

.PARAMETER Root
Parameter description

.EXAMPLE
Test-OSIsOnline -Root c:

This test return true on any Windows, except on WinPE

.EXAMPLE
Test-OSIsOnline -Root X:

This test return true only on WinPE

.NOTES
General notes
#>

function Test-OSIsOnline {
	[CmdletBinding()][OutputType([Boolean])]Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Root
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		# We want to check if we are online, so we can use Powershell's builtin $Is(Windows|MacOS|Linux) variables
		if ($IsWindows) {
			return $Root -eq $env:SystemDrive
		}
		if ($IsMacOS) {
			return $Root -eq '/'
		}
		if ($IsLinux) {
			return $Root -eq '/'
		}

		return $false
	}

	End {
		Write-LeaveFunction
	}
}
